webpackJsonp([1],{

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileListPageModule", function() { return ProfileListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_list__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileListPageModule = (function () {
    function ProfileListPageModule() {
    }
    return ProfileListPageModule;
}());
ProfileListPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__profile_list__["a" /* ProfileListPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_list__["a" /* ProfileListPage */]),
        ],
    })
], ProfileListPageModule);

//# sourceMappingURL=profile-list.module.js.map

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUF5QztBQUNPO0FBQ0M7QUFVakQsSUFBYSxxQkFBcUI7SUFBbEM7SUFBb0MsQ0FBQztJQUFELDRCQUFDO0FBQUQsQ0FBQztBQUF4QixxQkFBcUI7SUFSakMsdUVBQVEsQ0FBQztRQUNSLFlBQVksRUFBRTtZQUNaLHNFQUFlO1NBQ2hCO1FBQ0QsT0FBTyxFQUFFO1lBQ1Asc0VBQWUsQ0FBQyxRQUFRLENBQUMsc0VBQWUsQ0FBQztTQUMxQztLQUNGLENBQUM7R0FDVyxxQkFBcUIsQ0FBRztBQUFIIiwiZmlsZSI6IjEuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uaWNQYWdlTW9kdWxlIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBQcm9maWxlTGlzdFBhZ2UgfSBmcm9tICcuL3Byb2ZpbGUtbGlzdCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFByb2ZpbGVMaXN0UGFnZSxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIElvbmljUGFnZU1vZHVsZS5mb3JDaGlsZChQcm9maWxlTGlzdFBhZ2UpLFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBQcm9maWxlTGlzdFBhZ2VNb2R1bGUge31cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0Lm1vZHVsZS50cyJdLCJzb3VyY2VSb290IjoiIn0=