webpackJsonp([3],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__test_test__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_sqlite_db_sqlite_db__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_services_dialog__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the ProfileListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ProfileListPage = (function () {
    function ProfileListPage(navParams, loadingController, platform, sql, navCtrl, http, dialog) {
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.platform = platform;
        this.sql = sql;
        this.navCtrl = navCtrl;
        this.http = http;
        this.dialog = dialog;
        this.storedtest = [];
        this.user_id = null;
        this.loader = null;
        this.testType = ["T", "E", "Y", "S", "V", "F"];
        this.companyId = this.navParams.get('companyId');
        console.log("comapny Id", this.companyId);
    }
    ProfileListPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.platform.is('ios')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'ios'
            });
        }
        if (this.platform.is('android')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'dots'
            });
        }
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.tabBarElement.style.display = 'flex';
        console.log("ionViewDidEnter");
        if (typeof (localStorage.getItem("userLoggedIn")) != "undefined" && localStorage.getItem("userLoggedIn") != "false" && localStorage.getItem("userLoggedIn") != null) {
            this.loader.present().then(function () {
                _this.load();
            }).catch(function (errloader) {
                console.log(errloader.code);
            });
        }
    };
    ProfileListPage.prototype.ionViewWillEnter = function () {
        if (typeof (localStorage.getItem("userLoggedIn")) == "undefined" || localStorage.getItem("userLoggedIn") == "false" || localStorage.getItem("userLoggedIn") == null) {
            // this.navCtrl.push(LoginPage,{},()=>{console.log("displayed")});
            // this.app.getRootNav().push(LoginPage);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
            console.log("willEnter called");
        }
        console.log(localStorage.getItem("userLoggedIn"));
    };
    ProfileListPage.prototype.load = function () {
        var _this = this;
        console.log("Load called");
        var link = 'http://www.anytimelearn.in/maPages/getEnabledProfileIonic.php';
        var data = JSON.stringify({ simID: localStorage.getItem("deviceId"), companyProfile: this.companyId });
        this.sql.dbcreate('AnytimeLearn', ["CREATE TABLE IF NOT EXISTS assessment(ED TEXT, Name TEXT, Questions TEXT,TI TEXT PRIMARY KEY,TQ TEXT,TS TEXT)", []], function () { });
        this.sql.dbcreate('AnytimeLearn', ["CREATE TABLE IF NOT EXISTS submitresults(TI TEXT PRIMARY KEY,LINK VARCHAR(200),RESULTS VARCHAR(1000),RESPONSE VARCHAR(1000) DEFAULT -1)", []], function () { });
        this.http.post(link, data)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            // this.data.response = data.json;
            _this.posts = data;
            console.log(data);
            _this.loader.dismiss();
        }, function (error) {
            _this.loader.dismiss();
            console.log("Oooops!" + error);
            _this.query = "Select * from assessment";
            _this.temppost = _this.posts;
            _this.posts = [];
            _this.sql.dbcreate('AnytimeLearn', [_this.query, []], function (data) {
                for (var i = 0; i < data.rows.length; i++) {
                    _this.storedtest.push(data.rows.item(i).Name);
                }
                for (var i = 0; i < _this.temppost.length; i++) {
                    if (_this.storedtest.indexOf(_this.temppost[i]["c_cn"]) != -1)
                        _this.posts.push(_this.temppost[i]);
                }
                console.log(_this.posts);
            });
        });
    };
    ProfileListPage.prototype.loadTest = function (postTest) {
        var _this = this;
        console.log("postTest", postTest);
        if (postTest.c_nA == "1" && this.testType.indexOf(postTest.c_ct) != -1)
            this.dialog.displayDialog("Enter User Id", "", "Submit", "", function (text) {
                _this.user_id = text;
                if (text == "" || text.toString().match(/^[a-z0-9]+$/i) == null)
                    _this.dialog.dialogs.alert("Enter a valid User ID");
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__test_test__["a" /* TestPage */], {
                        "courseId": postTest.c_id,
                        "course_Name": postTest.c_cn,
                        "user_id": text,
                        "course_type": postTest.c_ct
                    });
                }
            });
        else if (this.testType.indexOf(postTest['c_ct']) != -1) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__test_test__["a" /* TestPage */], {
                "courseId": postTest.c_id,
                "course_Name": postTest.c_cn,
                "user_id": null,
                "course_type": postTest.c_ct
            });
        }
    };
    return ProfileListPage;
}());
ProfileListPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile-list',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/profile-list/profile-list.html"*/'<ion-header>\n	<ion-navbar color="danger">\n		<ion-title>My Courses</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content>\n	<ion-item *ngFor="let post of posts">\n	<ion-card (click)="loadTest(post)">\n		<ion-card-header><b>\n			{{post.c_cn}}\n		</b></ion-card-header>\n	</ion-card>\n	</ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/profile-list/profile-list.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__providers_sqlite_db_sqlite_db__["a" /* SqliteDbProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_7__providers_services_dialog__["a" /* DialogProvider */]])
], ProfileListPage);

//# sourceMappingURL=profile-list.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_services_toast__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_sqlite_db_sqlite_db__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_dialog__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the TestPage page.
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TestPage = (function () {
    function TestPage(dialog, platform, loadingController, sql, toast, navCtrl, navParams, http) {
        this.dialog = dialog;
        this.platform = platform;
        this.loadingController = loadingController;
        this.sql = sql;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.index = 0;
        this.currentPage = 1;
        this.answer = -1;
        this.answers = [];
        this.runtime = 0;
        this.multiselect_options = [];
        this.answer_multi = [];
        this.resultJSON = new Array();
        this.testDuration = 30000;
        this.submitted = false;
        this.quesNum = 0;
        this.totalPages = 0;
        this.showTimeTestType = ['E', 'S', 'V'];
        this.totalTime = "00:00:00";
        if (platform.is('ios')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'ios'
            });
        }
        if (platform.is('android')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'dots'
            });
        }
        if (typeof (localStorage.getItem("deviceId")) != 'undefined')
            this.device_id = localStorage.getItem("deviceId");
        else
            this.device_id = null;
    }
    TestPage.prototype.ngOnDestroy = function () {
        if (this.submitted == false) {
            this.submit(false);
            clearInterval(this.task);
            this.loader.dismiss();
        }
        else {
            console.log("On Destory called on test.ts");
            clearInterval(this.task);
            this.loader.dismiss();
        }
    };
    TestPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // works like intent gets the dats from course listing page 
        this.course_id = this.navParams.get('courseId');
        this.course_name = this.navParams.get('course_Name');
        this.user_id = this.navParams.get("user_id");
        this.courseType = this.navParams.get("course_type");
        console.log('ionViewDidLoad TestPage');
        //this.getTestAttributes(this.course_id);
        this.loader.present().then(function () {
            _this.getQuestion(_this.course_id, _this.quesNum);
            document.getElementById('loadnext').style.display = '';
            document.getElementById('submit').style.display = 'none';
        });
        clearInterval(this.task);
        if (this.showTimeTestType.indexOf(this.courseType) != -1) {
            clearInterval(this.task);
            this.task = setInterval(function () {
                _this.timeleft = _this.refreshData() + "/" + _this.totalTime;
            }, 1000);
        }
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.tabBarElement.style.display = 'none';
    };
    TestPage.prototype.refreshData = function () {
        this.runtime += 1000;
        if (this.runtime >= this.testDuration) {
            console.log("Test Done");
            this.submit(true);
            clearInterval(this.task);
            this.navCtrl.pop();
        }
        var millisec = this.runtime;
        var hour, minutes, seconds;
        if (millisec >= 3600000) {
            hour = '00' + Math.floor(millisec / (60 * 60 * 1000));
            millisec = millisec - (hour * 60 * 60 * 1000);
        }
        else
            hour = '00' + 0;
        if (millisec >= 60000) {
            minutes = '00' + Math.floor(millisec / (60 * 1000));
            millisec = millisec - (minutes * 60 * 1000);
        }
        else
            minutes = '00' + 0;
        if (millisec >= 1000) {
            seconds = '00' + Math.floor(millisec / 1000);
        }
        else
            seconds = '00' + 0;
        hour = hour.substr(hour.length - 2, hour.length - 1);
        minutes = minutes.substr(minutes.length - 2, minutes.length - 1);
        seconds = seconds.substr(seconds.length - 2, seconds.length - 1);
        return hour + ':' + minutes + ':' + seconds;
    };
    //get the question for temp, will move to provider page later
    TestPage.prototype.getQuestion = function (id, pageNo) {
        var _this = this;
        console.log(this.device_id);
        var link = 'http://www.anytimelearn.in/maPages/getTestAttribAndQuestionsIonic.php';
        var data = JSON.stringify({ CourseId: id, SimId: this.device_id });
        console.log(data);
        this.http.post(link, data)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            // this.data.response = data;
            _this.query = '  Insert OR Replace into assessment(ED,Name,Questions,TI,TQ,TS) Values(?,?,?,?,?,?)';
            _this.sql.dbcreate('AnytimeLearn', [_this.query, [data.list.ED, data.list.Name, JSON.stringify(data.list.Questions), data.list.TI, data.list.TQ, data.list.TS]], function () { });
            _this.displayDbData(id);
            console.log(data);
        }, function (error) {
            console.log("Oooops!" + error);
            _this.toast.showToast("Offline");
            _this.displayDbData(id);
        });
    };
    TestPage.prototype.displayDbData = function (id) {
        var _this = this;
        this.query = "Select * from assessment WHERE TI='" + id + "'";
        this.sql.dbcreate('AnytimeLearn', [this.query, []], function (data) {
            _this.resultdata = data.rows;
            for (var i = 0; i < _this.resultdata.item.length; i++) {
                console.log(_this.resultdata.item(i));
                try {
                    _this.resultdata.item(i).Questions = JSON.parse(_this.resultdata.item(i).Questions);
                }
                catch (err) {
                    _this.toast.showToast("Connection not available");
                    _this.ngOnDestroy();
                    _this.navCtrl.popToRoot();
                }
                var time = "00";
                time += Math.floor(Number(_this.resultdata.item(i).ED) / 60);
                time = time.substr(time.length - 2, time.length - 1);
                _this.totalTime = time;
                _this.test_id = _this.resultdata.item(i).TI;
                time = "00";
                time += Math.floor(Number(_this.resultdata.item(i).ED) - Number(_this.resultdata.item(i).ED) / 60);
                time = time.substr(time.length - 2, time.length - 1);
                _this.totalTime += ":" + time + ":00";
                _this.testDuration = Number(_this.resultdata.item(i).ED) * 60000;
                console.log("Duration:", _this.testDuration);
                _this.getQuesnList = _this.resultdata.item(i).Questions;
                _this.showCheckbox = _this.getQuesnList[_this.quesNum].rAns.split(",").length;
                console.log("length", _this.showCheckbox);
                _this.getQuesn = _this.getQuesnList[_this.quesNum].quen;
                _this.getOptn = _this.getQuesnList[_this.quesNum].optn;
                _this.totalPages = _this.getQuesnList.length;
                if (i == _this.resultdata.item.length - 1) {
                    for (var i = 0; i < _this.totalPages; i++)
                        _this.answers.push(-1);
                    console.log(data);
                }
            }
            _this.loader.dismiss();
        });
    };
    TestPage.prototype.getTestAttributes = function (id) {
        var link = 'http://www.anytimelearn.in/maPages/getTestAttributesIonic.php';
        var data = JSON.stringify({ CourseId: id, SimId: this.device_id });
        this.http.post(link, data)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            // this.data.response = data;
            //   console.log(data.expl);           
        }, function (error) {
            console.log("Oooops!" + error);
        });
    };
    TestPage.prototype.submit = function (doPop) {
        var _this = this;
        console.log(doPop);
        this.submitted = true;
        this.answers[this.quesNum] = this.answer;
        console.log(this.answers);
        var answerArray = new Array();
        for (var i = 0; i < this.totalPages; i++) {
            if (this.answers[i] != -1 && !Array.isArray(this.answers[i]))
                this.answers[i] = Number(this.answers[i]) + 1;
            if (this.answers[i] != -1 && Array.isArray(this.answers[i])) {
                this.answers[i] = this.answers[i].map(function (val, k) {
                    return Number(val) + 1;
                });
            }
            answerArray.push({
                "SeqNo": i,
                "PageNo": this.getQuesnList[i]["pageNum"],
                "Answer": Array.isArray(this.answers[i]) ? this.answers[i].join(":") : this.answers[i]
            });
        }
        this.resultJSON.push({
            "SimId": this.device_id,
            "CourseId": this.course_id,
            "UserId": this.user_id,
            "AnswerList": answerArray
        });
        console.log(this.resultJSON);
        console.log(JSON.stringify(this.answers));
        var link = "http://www.anytimelearn.in/maPages/takeExamAnswersIonic.php";
        if (this.platform.is('ios')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'ios'
            });
        }
        if (this.platform.is('android')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'dots'
            });
        }
        this.loader.dismiss();
        this.loader.present().then(function () {
            _this.http.post(link, JSON.stringify(_this.resultJSON))
                .subscribe(function (data) {
                console.log(data);
                if (data["_body"] == "Success") {
                    _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                    _this.sql.dbcreate('AnytimeLearn', [_this.query, [_this.test_id, link, JSON.stringify(_this.resultJSON), data["_body"]]], function () {
                        _this.loader.dismiss();
                        if (doPop)
                            _this.navCtrl.pop();
                    });
                }
                else {
                    if (data["_body"] == "ERROR") {
                        _this.toast.showToast("User not enrolled for test");
                        _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                        _this.sql.dbcreate('AnytimeLearn', [_this.query, [_this.test_id, link, JSON.stringify(_this.resultJSON), "-1"]], function () {
                            _this.loader.dismiss();
                            _this.toast.showToast("Something went wrong.Restart the app");
                            if (doPop)
                                _this.navCtrl.pop();
                        });
                    }
                    if (data["_body"] == "ERROR_INVALID") {
                        _this.toast.showToast("User not enrolled for test");
                        _this.loader.dismiss();
                        if (doPop)
                            _this.navCtrl.pop();
                    }
                }
            }, function (error) {
                _this.loader.dismiss();
                _this.toast.showToast("Something went wrong.Contact Admin");
                console.log("Oooops!" + error);
                _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                _this.sql.dbcreate('AnytimeLearn', [_this.query, [_this.test_id, link, JSON.stringify(_this.resultJSON), "-1"]], function () {
                    if (doPop)
                        _this.navCtrl.pop();
                });
            });
        });
    };
    //load next question
    TestPage.prototype.loadNextQuestion = function () {
        console.log(this.answer);
        if (this.answer == undefined)
            this.answer = -1;
        this.getQuesnList[this.quesNum].rAns.split(",").length;
        if (this.quesNum != this.totalPages - 1) {
            if (this.getQuesnList[this.quesNum].rAns.split(",").length > 1)
                this.answer = this.multiselect_options;
            this.answers[this.quesNum] = this.answer;
            console.log("before ++", this.answer);
            this.quesNum++;
            this.currentPage++;
            this.multiselect_options = [];
            this.showCheckbox = this.getQuesnList[this.quesNum].rAns.split(",").length;
            console.log(this.answer);
            console.log(this.answers);
            this.getQuesn = this.getQuesnList[this.quesNum].quen;
            this.getOptn = this.getQuesnList[this.quesNum].optn;
            this.answer = this.answers[this.quesNum];
            console.log("after ++", this.answer);
            if (Array.isArray(this.answer))
                this.multiselect_options = this.answer;
        }
        if (this.quesNum == this.totalPages - 1) {
            document.getElementById('submit').style.display = '';
            document.getElementById('loadnext').style.display = 'none';
        }
    };
    TestPage.prototype.multiselect = function (event) {
        if (this.multiselect_options.indexOf(event) != -1)
            this.multiselect_options.splice(this.multiselect_options.indexOf(event), 1);
        else
            this.multiselect_options.push(event);
        console.log(this.multiselect_options);
        console.log(this.answer);
    };
    //load previous question  
    TestPage.prototype.isChecked = function (val) {
        //   console.log("checking if is checked");
        if (this.multiselect_options.indexOf(val) != -1)
            return true;
        else
            return false;
    };
    TestPage.prototype.loadPreQuestion = function () {
        if (this.quesNum == 0)
            this.toast.showToast("You've reached the first question");
        if (this.quesNum == this.totalPages - 1) {
            this.answers[this.quesNum] = this.answer;
        }
        if (this.quesNum > 0) {
            document.getElementById('loadnext').style.display = '';
            document.getElementById('submit').style.display = 'none';
            console.log("before --", this.answer);
            this.quesNum--;
            this.currentPage--;
            this.showCheckbox = this.getQuesnList[this.quesNum].rAns.split(",").length;
            this.answer = this.answers[this.quesNum];
            console.log("after --", this.answer);
            if (this.getQuesnList[this.quesNum].rAns.split(",").length > 1)
                this.multiselect_options = this.answer;
            this.getQuesn = this.getQuesnList[this.quesNum].quen;
            this.getOptn = this.getQuesnList[this.quesNum].optn;
        }
    };
    return TestPage;
}());
TestPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-test',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/test/test.html"*/'<ion-header>\n	<ion-navbar>\n		<ion-title>{{course_name}}</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content padding>\n		<div align=\'right\' style="font-size:18px;font-weight: bolder;">&nbsp;<span style="float:left;">{{currentPage}}/{{totalPages}}</span><span style="float:right;"><ion-icon *ngIf="showTimeTestType.indexOf(courseType)!=-1" name="alarm"></ion-icon>&nbsp;{{timeleft}}</span></div>		\n	<ion-card >\n		<ion-card-header style="white-space: normal;">\n			{{getQuesn}}\n		</ion-card-header>\n	</ion-card>\n	<ion-list text-wrap radio-group [(ngModel)]="answer" selected> \n	<ion-scroll scrollY="true">\n		<div style="overflow:scroll;" *ngFor="let options of getOptn ; let i = index ">\n			<ion-item *ngIf="showCheckbox==1">\n				<ion-label>{{options.option}}</ion-label>\n				<ion-radio id=\'radioAll\'  value="{{i}}"  ></ion-radio>\n			</ion-item>\n			<ion-item *ngIf="showCheckbox>1">\n					<ion-label>{{options.option}}</ion-label>\n				<ion-checkbox id="checkboxAll" [checked]="isChecked(i)" (ionChange)="multiselect(i)"></ion-checkbox>\n			</ion-item>\n		</div>\n	</ion-scroll>\n	</ion-list>\n	<div ng-app="MyApplication" ng-controller="MyController">\n		<div ng-repeat="item in items track by $index" ng-show="$index == indexToShow">\n		{{item}}\n		</div>\n	</div>\n	<ion-footer>\n		<ion-toolbar>	\n			<ion-list>\n				<ion-item>\n					<button ion-button color="danger" icon-left (click)="loadPreQuestion()">\n						<ion-icon name="arrow-dropleft"></ion-icon>\n						Previous \n					</button>\n					<button  id=\'loadnext\' ion-button color="danger" item-right icon-right (click)="loadNextQuestion()">\n						Next\n						<ion-icon name="arrow-dropright"></ion-icon>\n					</button>\n					<button  id=\'submit\' ion-button color="primary" item-right icon-right (click)="submit(true)">\n						Submit\n						<ion-icon name="arrow-dropright"></ion-icon>\n					</button>\n					</ion-item>\n			</ion-list>\n		</ion-toolbar>\n	</ion-footer>\n</ion-content>'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/test/test.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_services_dialog__["a" /* DialogProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__providers_sqlite_db_sqlite_db__["a" /* SqliteDbProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_services_toast__["a" /* Toastservice */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]])
], TestPage);

//# sourceMappingURL=test.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Toastservice; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Toastservice = (function () {
    function Toastservice(toastCtrl) {
        this.toastCtrl = toastCtrl;
        console.log('toast called');
    }
    Toastservice.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Toast closed');
        });
        toast.dismissAll();
        toast.present();
    };
    return Toastservice;
}());
Toastservice = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* ToastController */]])
], Toastservice);

//# sourceMappingURL=toast.js.map

/***/ }),

/***/ 116:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 116;

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		278,
		2
	],
	"../pages/profile-list/profile-list.module": [
		277,
		1
	],
	"../pages/test/test.module": [
		279,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 158;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/tabs/tabs.html"*/'<ion-tabs id=\'tabs\'>\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="home"></ion-tab>\n  <!--<ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab> -->\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/tabs/tabs.html"*/
    }),
    __metadata("design:paramtypes", [])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/about/about.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactPage = (function () {
    function ContactPage(email, navCtrl) {
        var _this = this;
        this.email = email;
        this.navCtrl = navCtrl;
        this.emailAvailable = false;
        this.email.isAvailable().then(function (available) {
            if (available) {
                _this.emailAvailable = available;
            }
        });
    }
    ContactPage.prototype.sendEmail = function () {
        var emailData = {
            to: "iwanttolearn@anytimelearn.in",
            subject: "Query",
            body: "",
            isHtml: true
        };
        this.email.open(emailData);
    };
    return ContactPage;
}());
ContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item (click)="sendEmail()">\n      <ion-icon name="md-mail" item-left></ion-icon>\n      @Anytimelearn.in\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/contact/contact.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__["a" /* EmailComposer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_profile_list_profile_list__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_services__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(platform, http, loadingController, navCtrl, services) {
        this.platform = platform;
        this.http = http;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.services = services;
        this.displayProfiles = new Array();
        clearInterval(this.interval);
        this.interval = setInterval(this.services.httpBackgorundResultPost(), 10000);
        //remember to put the below along with present else not displayed aftr tab change
    }
    HomePage.prototype.ionViewWillEnter = function () {
        console.log("Ion view will enter");
        if (typeof (localStorage.getItem("userLoggedIn")) == "undefined" || localStorage.getItem("userLoggedIn") == "false" || localStorage.getItem("userLoggedIn") == null) {
            // this.navCtrl.push(LoginPage,{},()=>{console.log("displayed")});
            // this.app.getRootNav().push(LoginPage);
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
            console.log("willEnter called");
        }
    };
    HomePage.prototype.load = function () {
        var _this = this;
        if (window.localStorage.getItem("testTaken") != null)
            window.localStorage.setItem("testTaken", "");
        this.displayProfiles = new Array();
        var link = "http://www.anytimelearn.in/maPages/profileListIonic.php";
        this.http.post(link, JSON.stringify({ simId: localStorage.getItem("deviceId") }))
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.profiles = data;
            if (typeof (_this.profiles) != "undefined") {
                _this.loader.dismiss();
                for (var i = 0; i < _this.profiles.profileList.length; i++) {
                    _this.profiles.profileList[i]["image"] = _this.profiles.url + _this.profiles.profileList[i].company_id + ".png";
                    _this.displayProfiles.push(_this.profiles.profileList[i]);
                }
                console.log(_this.displayProfiles);
            }
        }, function (error) {
            console.log("Ooops Error in getting profiles");
        });
    };
    HomePage.prototype.submit = function (companyId) {
        console.log(companyId);
        if (typeof (companyId) != "undefined" || companyId != null)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_profile_list_profile_list__["a" /* ProfileListPage */], {
                "companyId": companyId
            });
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        if (this.platform.is('ios')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'ios'
            });
        }
        if (this.platform.is('android')) {
            this.loader = this.loadingController.create({
                content: "Please Wait..",
                showBackdrop: true,
                spinner: 'dots'
            });
        }
        console.log("Ion view did enter");
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.tabBarElement.style.display = 'flex';
        if (typeof (localStorage.getItem("userLoggedIn")) != "undefined" && localStorage.getItem("userLoggedIn") != "false" && localStorage.getItem("userLoggedIn") != null) {
            this.loader.present().then(function () {
                _this.load();
            }).catch(function (errloader) {
                console.log(errloader.code);
            });
        }
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/home/home.html"*/'<ion-header>\n	<ion-navbar color="danger">\n		<ion-title>Profiles</ion-title>\n	</ion-navbar>\n</ion-header>\n<ion-content >\n	<ion-grid>\n		<ion-row> \n			<ion-col *ngFor="let profile of displayProfiles" col-6>\n					<ion-card style="box-shadow: 7px 7px 3px darkslategray;" (click)="submit(profile.company_id)">\n								<ion-card-header style="min-height:120px;max-height:120px;white-space: normal;">\n								<img src="{{profile.image}}" alt="Image" onerror="this.src=\'assets/imgs/atlicon.png\';">\n								</ion-card-header>\n								<ion-card-content style="min-height:30px;max-height:30px;margin-bottom:3%;">\n										<h5 style="text-align:center;padding-bottom:2%;font-family:\'Times New Roman\', Times, serif;">{{profile.company_name}}</h5>\n								</ion-card-content>\n							</ion-card>\n				</ion-col>\n			</ion-row>\n	</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_services_services__["a" /* ServicesProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_sqlite_db_sqlite_db__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServicesProvider = (function () {
    function ServicesProvider(http, sqlite) {
        this.http = http;
        this.sqlite = sqlite;
        console.log('Hello ServicesProvider Provider');
    }
    ServicesProvider.prototype.httpBackgorundResultPost = function () {
        var _this = this;
        this.sqlite.dbcreate('AnytimeLearn', ["Select * from submitresults where RESPONSE='-1'", []], function (data) {
            _this.resultdata = data.rows;
            for (var i = 0; i < _this.resultdata.item.length; i++) {
                console.log("from services", _this.resultdata.item(i));
                _this.http.post(_this.resultdata.item(i).LINK, _this.resultdata.item(i).RESULTS).subscribe(function (data) {
                    console.log(data);
                    if (data["_body"] == "Success") {
                        _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                        _this.sqlite.dbcreate('AnytimeLearn', [_this.query, [_this.resultdata.item(i).TI, _this.resultdata.item(i).LINK, _this.resultdata.item(i).RESULTS, data["_body"]]], function () {
                        });
                    }
                    else {
                        console.log(data);
                        _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                        _this.sqlite.dbcreate('AnytimeLearn', [_this.query, [_this.resultdata.item(i).TI, _this.resultdata.item(i).LINK, _this.resultdata.item(i).RESULTS, "-1"]], function () {
                        });
                    }
                }, function (error) {
                    console.log("Ooops error");
                    _this.query = '  Insert OR Replace into submitresults(TI,LINK,RESULTS,RESPONSE) Values(?,?,?,?)';
                    _this.sqlite.dbcreate('AnytimeLearn', [_this.query, [_this.resultdata.item(i).TI, _this.resultdata.item(i).LINK, _this.resultdata.item(i).RESULTS, "-1"]], function () {
                    });
                });
            }
        });
    };
    return ServicesProvider;
}());
ServicesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__providers_sqlite_db_sqlite_db__["a" /* SqliteDbProvider */]])
], ServicesProvider);

//# sourceMappingURL=services.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(228);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_about_about__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_test_test__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_services_services__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_sqlite_db_sqlite_db__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_sqlite__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_services_toast__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_services_dialog__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_dialogs__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_unique_device_id__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_profile_list_profile_list__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_email_composer__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_profile_list_profile_list__["a" /* ProfileListPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_test_test__["a" /* TestPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/profile-list/profile-list.module#ProfileListPageModule', name: 'ProfileListPage', segment: 'profile-list', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/test/test.module#TestPageModule', name: 'TestPage', segment: 'test', priority: 'low', defaultHistory: [] }
                ]
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_contact_contact__["a" /* ContactPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_profile_list_profile_list__["a" /* ProfileListPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_test_test__["a" /* TestPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_unique_device_id__["a" /* UniqueDeviceID */],
            __WEBPACK_IMPORTED_MODULE_15__providers_services_toast__["a" /* Toastservice */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_12__providers_services_services__["a" /* ServicesProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_sqlite_db_sqlite_db__["a" /* SqliteDbProvider */],
            __WEBPACK_IMPORTED_MODULE_16__providers_services_dialog__["a" /* DialogProvider */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_email_composer__["a" /* EmailComposer */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_dialogs__["a" /* Dialogs */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SqliteDbProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the SqliteDbProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var SqliteDbProvider = (function () {
    function SqliteDbProvider(sqlite) {
        this.sqlite = sqlite;
    }
    SqliteDbProvider.prototype.dbcreate = function (dbname, query, callback) {
        this.sqlite.create({
            name: dbname + '.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(query[0], query[1]).then(function (data) { console.log("SQL exec successfull", data); callback(data); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    return SqliteDbProvider;
}());
SqliteDbProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_sqlite__["a" /* SQLite */]])
], SqliteDbProvider);

//# sourceMappingURL=sqlite-db.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_dialogs__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DialogProvider = (function () {
    function DialogProvider(dialogs) {
        this.dialogs = dialogs;
    }
    DialogProvider.prototype.displayDialog = function (message, title, buttonLabel, hint, callback) {
        this.dialogs.prompt(message, title, buttonLabel, hint)
            .then(function (text) { console.log("Dialog executed", text); callback(text.input1); })
            .catch(function (err) { console.log("Dialog error"); });
    };
    DialogProvider.prototype.displayConfirm = function (message, title, buttonLabel, callback) {
        this.dialogs.confirm(message, title, buttonLabel)
            .then(function (clickedOption) {
            console.log('Clicked is', clickedOption);
            callback(clickedOption);
        })
            .catch(function (err) { console.log("Error occured in confirm"); });
    };
    return DialogProvider;
}());
DialogProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_native_dialogs__["a" /* Dialogs */]])
], DialogProvider);

//# sourceMappingURL=dialog.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_unique_device_id__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_services_dialog__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
*/
var LoginPage = (function () {
    function LoginPage(dialog, app, deviceId, http, navCtrl, navParams) {
        this.dialog = dialog;
        this.app = app;
        this.deviceId = deviceId;
        this.http = http;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.phoneNumber_wrong = true;
        this.email_wrong = true;
        this.disable_signin = false;
        this.typesOfUsers = ["I'm a Student", "I'm a employee", "I'm a general user"];
        console.log("constructor");
    }
    LoginPage.prototype.submitCompanyData = function (val) {
        var _this = this;
        console.log(val);
        var link = 'http://www.anytimelearn.in/maPages/deviceRegistrationIonic.php';
        var dataGen = {
            simId: localStorage.getItem("deviceId"),
            emailId: "",
            ownPhone: "",
            companyUniqueId: 0,
            company_id: this.company_id
        };
        switch (Number(val)) {
            case 1:
                if (this.email_wrong != true && this.phoneNumber_wrong != true) {
                    dataGen.emailId = this.username;
                    dataGen.ownPhone = this.phoneNumber;
                }
                else
                    return;
                break;
            case 2:
                if (this.phoneNumber_wrong != true) {
                    dataGen.emailId = this.phoneNumber + "@" + this.company_id;
                    dataGen.ownPhone = this.phoneNumber;
                }
                else
                    return;
                break;
            case 3:
                if (this.phoneNumber_wrong != true) {
                    dataGen.emailId = this.username + "@" + this.company_id;
                    dataGen.ownPhone = this.phoneNumber;
                }
                else
                    return;
                break;
            case 4:
                if (this.phoneNumber_wrong != true) {
                    dataGen.emailId = this.username + "@" + this.company_id;
                    dataGen.ownPhone = this.phoneNumber;
                }
                else
                    return;
                break;
        }
        this.deviceId.get()
            .then(function (Id) {
            localStorage.setItem("deviceId", Id);
            dataGen.simId = Id;
            var data = JSON.stringify(dataGen);
            console.log(dataGen);
            _this.http.post(link, data)
                .subscribe(function (data) {
                console.log(data);
                if (data["_body"] == "Success") {
                    localStorage.setItem("userLoggedIn", "true");
                    _this.navCtrl.pop();
                }
                _this.username = '';
                _this.phoneNumber = '';
            }, function (error) {
                console.log("Oooops!" + error);
                localStorage.setItem("userLoggedIn", "false");
                _this.username = '';
                _this.phoneNumber = '';
            });
        })
            .catch(function (err) { return console.log("Error getting deviceId"); });
    };
    LoginPage.prototype.radioChangeHandler = function (event) {
        var _this = this;
        console.log(event);
        switch (Number(event)) {
            case 0:
                document.getElementById('displayList').style.display = 'none';
                document.getElementById('SignIn').style.display = '';
                break;
            case 1:
                this.dialog.displayConfirm('Do you have an Company ID', 'Message', ["Yes", "No"], function (clicked) {
                    switch (Number(clicked)) {
                        case 0:
                            break;
                        case 1:
                            _this.setDisplayForm('EmployeeSignIn');
                            break;
                        case 2:
                            break;
                    }
                    console.log("Confirm completed");
                });
                break;
            case 2:
                document.getElementById('displayList').style.display = 'none';
                document.getElementById('SignInUser').style.display = '';
                break;
        }
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
        document.getElementById('SignIn').style.display = 'none';
        document.getElementById('SignInUser').style.display = 'none';
        document.getElementById("EmployeeSignIn").style.display = 'none';
        document.getElementById("CompanySignIn1").style.display = 'none';
        document.getElementById("CompanySignIn2").style.display = 'none';
        document.getElementById("CompanySignIn3").style.display = 'none';
        document.getElementById("CompanySignIn4").style.display = 'none';
        this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
        this.tabBarElement.style.display = 'none';
    };
    LoginPage.prototype.submit = function (val) {
        var _this = this;
        switch (Number(val)) {
            case 0:
            case 1:
                if (this.email_wrong != true && this.phoneNumber_wrong != true) {
                    var link = 'http://www.anytimelearn.in/maPages/deviceRegistrationIonic.php';
                    this.deviceId.get()
                        .then(function (Id) {
                        localStorage.setItem("deviceId", Id);
                        var data = JSON.stringify({
                            simId: localStorage.getItem("deviceId"),
                            emailId: _this.username,
                            ownPhone: _this.phoneNumber,
                            companyUniqueId: 0,
                            company_id: 0
                        });
                        console.log(data);
                        _this.http.post(link, data)
                            .subscribe(function (data) {
                            if (data["_body"] == "Success") {
                                localStorage.setItem("userLoggedIn", "true");
                                console.log("Data !" + data);
                                _this.navCtrl.pop();
                            }
                            _this.username = '';
                            _this.phoneNumber = '';
                        }, function (error) {
                            console.log("Oooops!" + error);
                            localStorage.setItem("userLoggedIn", "false");
                            _this.username = '';
                            _this.phoneNumber = '';
                        });
                    })
                        .catch(function (err) { return console.log("Error getting deviceId"); });
                }
                break;
            case 2:
                console.log("username", this.username);
                if (this.username != " " || typeof (this.username) != null) {
                    console.log("exec started");
                    var link = 'http://www.anytimelearn.in/maPages/getRegistrationFiledsIonic.php';
                    this.company_id = this.username;
                    var data = JSON.stringify({
                        companyId: this.username
                    });
                    this.http.post(link, data)
                        .subscribe(function (data) {
                        console.log("exec", data);
                        _this.setDisplayForm("CompanySignIn" + data["_body"]);
                        _this.username = '';
                        _this.phoneNumber = '';
                    }, function (error) {
                        console.log("Oooops!" + error);
                        _this.username = '';
                        _this.phoneNumber = '';
                    });
                }
                break;
        }
    };
    LoginPage.prototype.usernamecheck = function (val) {
        switch (Number(val)) {
            case 0:
                if (!this.username.match(/[a-z0-9]+[@]{1}[a-z0-9]+[.]{1}[a-z0-9]+/i)) {
                    document.getElementById('emailid').style.borderColor = "red";
                    this.email_wrong = true;
                }
                else {
                    document.getElementById('emailid').style.borderColor = "white";
                    this.email_wrong = false;
                }
                break;
            case 1:
                if (!this.phoneNumber.match(/^[0-9]{10}$/)) {
                    this.phoneNumber_wrong = true;
                    document.getElementById('pass_doc').style.borderColor = "red";
                }
                else {
                    document.getElementById('pass_doc').style.borderColor = "white";
                    this.phoneNumber_wrong = false;
                }
                break;
        }
    };
    LoginPage.prototype.setDisplayForm = function (val) {
        if (val == "displayList") {
            console.log("SignIN");
            document.getElementById("SignIn").style.display = 'none';
            document.getElementById("displayList").style.display = '';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = 'none';
            this.disable_signin = true;
        }
        else if (val == "displayListUser") {
            console.log("SignINUser");
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = '';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = 'none';
            this.disable_signin = true;
        }
        else if (val == "EmployeeSignIn") {
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = 'none';
            document.getElementById("EmployeeSignIn").style.display = '';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = 'none';
        }
        else if (val == "CompanySignIn1") {
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = 'none';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = '';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = 'none';
        }
        else if (val == "CompanySignIn2") {
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = 'none';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = '';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = 'none';
        }
        else if (val == "CompanySignIn3") {
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = 'none';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = '';
            document.getElementById("CompanySignIn4").style.display = 'none';
        }
        else if (val == "CompanySignIn4") {
            document.getElementById("SignInUser").style.display = 'none';
            document.getElementById("displayList").style.display = 'none';
            document.getElementById("EmployeeSignIn").style.display = 'none';
            document.getElementById("CompanySignIn1").style.display = 'none';
            document.getElementById("CompanySignIn2").style.display = 'none';
            document.getElementById("CompanySignIn3").style.display = 'none';
            document.getElementById("CompanySignIn4").style.display = '';
        }
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header >\n  \n    <ion-navbar  hideBackButton="true" color="danger" >\n      <ion-title text-center style="font-size: 30px;"><b>Anytime Learn</b></ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content style=" padding: 0px;">    \n    <div text-center style="margin-top:10%;">\n    <img src="assets/imgs/atlicon.png" alt="AnytimeLearn logo"  width=50% height=50%>\n    <h1 style="color:#ffa726;font-size:30px;font-family:Summit;font-style:normal;"><b><span style="color:black">A</span>ny<span style="color:black"> T</span>ime<span style="color:black"> L</span>earn</b></h1>\n    </div>\n<div id="displayList" style="bottom: 0.5%;\nposition: absolute;margin-top:5%;\nwidth: 100%;">\n    <ion-list radio-group (ionChange)="radioChangeHandler($event)" > \n     \n      <div  *ngFor="let options of typesOfUsers ; let i = index ">\n          <br />\n        <ion-item style="background-color:#ffa726;" >\n          <ion-label >{{options}}</ion-label>\n          <ion-radio id=\'radioAll\'  value="{{i}}"  ></ion-radio>\n        </ion-item>\n      </div>\n    </ion-list>\n  </div>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="SignIn" disabled="disable_signin"  (ngSubmit)="submit(0)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid" [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="email" name="username" placeholder="Enter Email" (ionChange)="usernamecheck(0)" required ></ion-input>\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>\n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Register</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a student?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%; \n  width: 100%;" id="SignInUser" disabled="disable_signin"  (ngSubmit)="submit(1)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid" [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="email" name="username" placeholder="Enter Email" (ionChange)="usernamecheck(0)" required ></ion-input>\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>\n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Register</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayListUser\')">Not a General User?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="EmployeeSignIn" disabled="disable_signin"  (ngSubmit)="submit(2)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid" [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="text" name="username" placeholder="Enter Company Id" required ></ion-input>\n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Proceed</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a employee?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="CompanySignIn3" disabled="disable_signin"  (ngSubmit)="submitCompanyData(3)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid"  [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="text" name="username" placeholder="Enter Employee Id" required ></ion-input>\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>  \n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Proceed</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a employee?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="CompanySignIn2" disabled="disable_signin"  (ngSubmit)="submitCompanyData(2)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>  \n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Proceed</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a employee?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="CompanySignIn1" disabled="disable_signin"  (ngSubmit)="submitCompanyData(1)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid"  [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="text" name="username" (ionChange)="usernamecheck(0)" placeholder="Enter email Id" required ></ion-input>\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>  \n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Proceed</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a employee?</h5>\n  </form>\n  <form style="bottom: 0.5%;\n  position: absolute;margin-top:5%;\n  width: 100%;" id="CompanySignIn4" disabled="disable_signin"  (ngSubmit)="submitCompanyData(4)">\n    <div text-center  style="background-color:#ffa726;">\n      <ion-input id="emailid"  [(ngModel)]="username" style="border:1px solid white;color:darkslategray;background-color:white;" type="text" name="username" placeholder="Enter Login Id" required ></ion-input>\n      <ion-input id="pass_doc" [(ngModel)]="phoneNumber" style="border:1px solid white;margin-top:1%;color:darkslategray;background-color:white;" type="number" name="phoneNumber" placeholder="Enter Phone Number" (ionChange)="usernamecheck(1)" required ></ion-input>  \n    </div>\n    <button  type="submit" style="margin-top:5%;" ion-button block style="background-color:#ffa726;">Proceed</button>\n    <h5 text-center style="margin-top:5%;font-style:normal; font-family:\'Franklin Gothic Medium\', \'Arial Narrow\', Arial, sans-serif; color:#f64f31;text-decoration:underline;" (click)="setDisplayForm(\'displayList\')">Not a employee?</h5>\n  </form>\n</ion-content>\n  '/*ion-inline-end:"/Users/vinayprithiani/Desktop/atl_ionic/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_services_dialog__["a" /* DialogProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* App */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_unique_device_id__["a" /* UniqueDeviceID */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

},[209]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0LnRzIiwiLi4vLi4vc3JjL3BhZ2VzL3Rlc3QvdGVzdC50cyIsIi4uLy4uL3NyYy9wcm92aWRlcnMvc2VydmljZXMvdG9hc3QudHMiLCIuLi8uLi9ub2RlX21vZHVsZXMvQGFuZ3VsYXIvY29yZS9AYW5ndWxhciBsYXp5IiwiLi4vLi4vc3JjIGxhenkiLCIuLi8uLi9zcmMvcGFnZXMvdGFicy90YWJzLnRzIiwiLi4vLi4vc3JjL3BhZ2VzL2Fib3V0L2Fib3V0LnRzIiwiLi4vLi4vc3JjL3BhZ2VzL2NvbnRhY3QvY29udGFjdC50cyIsIi4uLy4uL3NyYy9wYWdlcy9ob21lL2hvbWUudHMiLCIuLi8uLi9zcmMvcHJvdmlkZXJzL3NlcnZpY2VzL3NlcnZpY2VzLnRzIiwiLi4vLi4vc3JjL2FwcC9tYWluLnRzIiwiLi4vLi4vc3JjL2FwcC9hcHAubW9kdWxlLnRzIiwiLi4vLi4vc3JjL2FwcC9hcHAuY29tcG9uZW50LnRzIiwiLi4vLi4vc3JjL3Byb3ZpZGVycy9zcWxpdGUtZGIvc3FsaXRlLWRiLnRzIiwiLi4vLi4vc3JjL3Byb3ZpZGVycy9zZXJ2aWNlcy9kaWFsb2cudHMiLCIuLi8uLi9zcmMvcGFnZXMvbG9naW4vbG9naW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUEwQztBQUNlO0FBQ3BCO0FBQ0c7QUFDRztBQUNaO0FBQ3NDO0FBQ047QUFDZjtBQUNQO0FBQ3pDOzs7OztHQUtHO0FBT0gsSUFBYSxlQUFlO0lBYTFCLHlCQUFtQixTQUFtQixFQUFRLGlCQUFtQyxFQUFRLFFBQWlCLEVBQVMsR0FBb0IsRUFBUSxPQUFzQixFQUFTLElBQVUsRUFBUSxNQUFxQjtRQUFsTSxjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQVEsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUFRLGFBQVEsR0FBUixRQUFRLENBQVM7UUFBUyxRQUFHLEdBQUgsR0FBRyxDQUFpQjtRQUFRLFlBQU8sR0FBUCxPQUFPLENBQWU7UUFBUyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVEsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQVJyTixlQUFVLEdBQUssRUFBRSxDQUFDO1FBR2xCLFlBQU8sR0FBSyxJQUFJLENBQUM7UUFDakIsV0FBTSxHQUFLLElBQUksQ0FBQztRQUdoQixhQUFRLEdBQVUsQ0FBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsRUFBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDRCx5Q0FBZSxHQUFmO1FBQUEsaUJBeUJDO1FBeEJDLEVBQUUsRUFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFDO1lBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQztnQkFDeEMsT0FBTyxFQUFDLGVBQWU7Z0JBQ3ZCLFlBQVksRUFBQyxJQUFJO2dCQUNqQixPQUFPLEVBQUMsS0FBSzthQUNkLENBQUMsQ0FBQztRQUNMLENBQUM7UUFDRCxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBQztZQUM5QixJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hDLE9BQU8sRUFBQyxlQUFlO2dCQUN2QixZQUFZLEVBQUMsSUFBSTtnQkFDakIsT0FBTyxFQUFDLE1BQU07YUFDZixDQUFDLENBQUM7UUFDTCxDQUFDO1FBQ0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDL0IsRUFBRSxFQUFDLE9BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUUsV0FBVyxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUUsT0FBTyxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUUsSUFBSyxDQUFDLEVBQUM7WUFDNUosSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNkLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLFNBQVM7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztJQUNILENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEI7UUFDRSxFQUFFLEVBQUMsT0FBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBRSxXQUFXLElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBRSxPQUFPLElBQUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBRSxJQUFLLENBQUMsRUFBQztZQUMzSixrRUFBa0U7WUFDbEUseUNBQXlDO1lBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLCtEQUFTLENBQUMsQ0FBQztZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDakMsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFDSCw4QkFBSSxHQUFKO1FBQUEsaUJBK0JLO1FBOUJILE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckIsSUFBSSxJQUFJLEdBQUcsK0RBQStELENBQUM7UUFDM0UsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFDLEtBQUssRUFBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFDLGNBQWMsRUFBQyxJQUFJLENBQUMsU0FBUyxFQUFDLENBQUMsQ0FBQztRQUNsRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQywrR0FBK0csRUFBQyxFQUFFLENBQUMsRUFBQyxjQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzlKLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBQyxDQUFDLHlJQUF5SSxFQUFDLEVBQUUsQ0FBQyxFQUFDLGNBQUssQ0FBQyxDQUFDLENBQUM7UUFDeEwsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQzthQUN6QixHQUFHLENBQUMsYUFBRyxJQUFJLFVBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBVixDQUFVLENBQUM7YUFDdEIsU0FBUyxDQUFDLGNBQUk7WUFDWixrQ0FBa0M7WUFDbEMsS0FBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDVixDQUFDLEVBQUUsZUFBSztZQUVwQixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlCLEtBQUksQ0FBQyxLQUFLLEdBQUMsMEJBQTBCLENBQUM7WUFDdEMsS0FBSSxDQUFDLFFBQVEsR0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxLQUFLLEdBQUMsRUFBRSxDQUFDO1lBQ2QsS0FBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssRUFBQyxFQUFFLENBQUMsRUFBQyxVQUFDLElBQUk7Z0JBQ3BELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQyxDQUFDO2dCQUNELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7b0JBQ3RDLEVBQUUsRUFBQyxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ3RELEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsQ0FBQztnQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVKLGtDQUFRLEdBQVIsVUFBUyxRQUFRO1FBQWpCLGlCQXdCRTtRQXZCQSxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBQyxRQUFRLENBQUMsQ0FBQztRQUNoQyxFQUFFLEVBQUMsUUFBUSxDQUFDLElBQUksSUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFFLENBQUMsQ0FBRSxDQUFDO1lBQ2pFLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBQyxFQUFFLEVBQUMsUUFBUSxFQUFDLEVBQUUsRUFBQyxVQUFDLElBQUk7Z0JBQzVELEtBQUksQ0FBQyxPQUFPLEdBQUMsSUFBSSxDQUFDO2dCQUNsQixFQUFFLEVBQUMsSUFBSSxJQUFFLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFFLElBQUksQ0FBQztvQkFDekQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQ3JELElBQUksRUFBQztvQkFDSCxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyw0REFBUSxFQUFFO3dCQUMxQixVQUFVLEVBQUMsUUFBUSxDQUFDLElBQUk7d0JBQ3hCLGFBQWEsRUFBQyxRQUFRLENBQUMsSUFBSTt3QkFDM0IsU0FBUyxFQUFDLElBQUk7d0JBQ2QsYUFBYSxFQUFDLFFBQVEsQ0FBQyxJQUFJO3FCQUM1QixDQUFDO2dCQUNKLENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNOLElBQUksQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUUsQ0FBQyxDQUFDLENBQUMsRUFBQztZQUNwRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyw0REFBUSxFQUFFO2dCQUMxQixVQUFVLEVBQUMsUUFBUSxDQUFDLElBQUk7Z0JBQ3hCLGFBQWEsRUFBQyxRQUFRLENBQUMsSUFBSTtnQkFDM0IsU0FBUyxFQUFDLElBQUk7Z0JBQ2QsYUFBYSxFQUFDLFFBQVEsQ0FBQyxJQUFJO2FBQzVCLENBQUM7UUFDSixDQUFDO0lBQ0QsQ0FBQztJQUdILHNCQUFDO0FBQUQsQ0FBQztBQWpIWSxlQUFlO0lBSjNCLHdFQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO09BQ0c7S0FDakMsQ0FBQztvQkFjcU47QUFvR3ROO1NBakhZLGVBQWUsZ0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEJ3QjtBQUNLO0FBQ3BCO0FBQ047QUFDNkI7QUFDUztBQUdyQjtBQUNQO0FBQ3NCO0FBQy9EOzs7O0dBSUc7QUFRSCxJQUFhLFFBQVE7SUFpQ25CLGtCQUFtQixNQUFxQixFQUFRLFFBQWlCLEVBQVEsaUJBQW1DLEVBQVEsR0FBb0IsRUFBUSxLQUFrQixFQUFRLE9BQXNCLEVBQVMsU0FBb0IsRUFBUSxJQUFVO1FBQTVOLFdBQU0sR0FBTixNQUFNLENBQWU7UUFBUSxhQUFRLEdBQVIsUUFBUSxDQUFTO1FBQVEsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUFRLFFBQUcsR0FBSCxHQUFHLENBQWlCO1FBQVEsVUFBSyxHQUFMLEtBQUssQ0FBYTtRQUFRLFlBQU8sR0FBUCxPQUFPLENBQWU7UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVEsU0FBSSxHQUFKLElBQUksQ0FBTTtRQWhDL08sVUFBSyxHQUFXLENBQUMsQ0FBQztRQU1sQixnQkFBVyxHQUFNLENBQUMsQ0FBQztRQUNuQixXQUFNLEdBQUssQ0FBQyxDQUFDLENBQUM7UUFDZCxZQUFPLEdBQUssRUFBRSxDQUFDO1FBQ2YsWUFBTyxHQUFRLENBQUMsQ0FBQztRQUtqQix3QkFBbUIsR0FBSyxFQUFFLENBQUM7UUFFM0IsaUJBQVksR0FBSyxFQUFFLENBQUM7UUFLcEIsZUFBVSxHQUFPLElBQUksS0FBSyxFQUFFLENBQUM7UUFHN0IsaUJBQVksR0FBUSxLQUFLLENBQUM7UUFDMUIsY0FBUyxHQUFTLEtBQUssQ0FBQztRQUNqQixZQUFPLEdBQVUsQ0FBQyxDQUFDO1FBQ2xCLGVBQVUsR0FBVSxDQUFDLENBQUM7UUFFOUIscUJBQWdCLEdBQVUsQ0FBQyxHQUFHLEVBQUMsR0FBRyxFQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hDLGNBQVMsR0FBUSxVQUFVLENBQUM7UUFHMUIsRUFBRSxFQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBQztZQUN2QixJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hDLE9BQU8sRUFBQyxlQUFlO2dCQUN2QixZQUFZLEVBQUMsSUFBSTtnQkFDakIsT0FBTyxFQUFDLEtBQUs7YUFDZCxDQUFDLENBQUM7UUFDTCxDQUFDO1FBQ0QsRUFBRSxFQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBQztZQUN6QixJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hDLE9BQU8sRUFBQyxlQUFlO2dCQUN2QixZQUFZLEVBQUMsSUFBSTtnQkFDakIsT0FBTyxFQUFDLE1BQU07YUFDZixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUMsRUFBRSxFQUFDLE9BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUUsV0FBVyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNuRCxJQUFJO1lBQ0YsSUFBSSxDQUFDLFNBQVMsR0FBQyxJQUFJLENBQUM7SUFDdEIsQ0FBQztJQUVILDhCQUFXLEdBQVg7UUFDRSxFQUFFLEVBQUMsSUFBSSxDQUFDLFNBQVMsSUFBRSxLQUFLLENBQUMsRUFBQztZQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDO1FBQUEsSUFBSSxFQUFDO1lBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1lBQzFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDO0lBQ0QsQ0FBQztJQUVILGlDQUFjLEdBQWQ7UUFBQSxpQkFzQkM7UUFyQkMsNERBQTREO1FBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsT0FBTyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ3ZDLHlDQUF5QztRQUN6QyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlDLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxFQUFFLENBQUM7WUFDckQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUN2RCxDQUFDLENBQUMsQ0FBQztRQUNILGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsRUFBRSxFQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFFLENBQUMsQ0FBQyxDQUFDLENBQ3BELENBQUM7WUFDSCxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO2dCQUN0QixLQUFJLENBQUMsUUFBUSxHQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsR0FBQyxHQUFHLEdBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQztZQUN0RCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFBQSxDQUFDO1FBQ1YsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztJQUM1QyxDQUFDO0lBR0QsOEJBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxPQUFPLElBQUUsSUFBSSxDQUFDO1FBQ25CLEVBQUUsRUFBQyxJQUFJLENBQUMsT0FBTyxJQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBQztZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLENBQUM7UUFDRCxJQUFJLFFBQVEsR0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzFCLElBQUksSUFBSSxFQUFDLE9BQU8sRUFBQyxPQUFPLENBQUM7UUFDekIsRUFBRSxFQUFDLFFBQVEsSUFBRSxPQUFPLENBQUMsRUFBQztZQUNwQixJQUFJLEdBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFDLENBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzVDLFFBQVEsR0FBQyxRQUFRLEdBQUMsQ0FBQyxJQUFJLEdBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxDQUFDO1FBQ0QsSUFBSTtZQUNGLElBQUksR0FBQyxJQUFJLEdBQUMsQ0FBQyxDQUFDO1FBQ2QsRUFBRSxFQUFDLFFBQVEsSUFBRSxLQUFLLENBQUMsRUFBQztZQUNsQixPQUFPLEdBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFDLENBQUMsRUFBRSxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDNUMsUUFBUSxHQUFDLFFBQVEsR0FBQyxDQUFDLE9BQU8sR0FBQyxFQUFFLEdBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUNELElBQUk7WUFBQyxPQUFPLEdBQUMsSUFBSSxHQUFDLENBQUMsQ0FBQztRQUNwQixFQUFFLEVBQUMsUUFBUSxJQUFFLElBQUksQ0FBQyxFQUFDO1lBQ2pCLE9BQU8sR0FBQyxJQUFJLEdBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLENBQUM7UUFDekMsQ0FBQztRQUNELElBQUk7WUFDRixPQUFPLEdBQUMsSUFBSSxHQUFDLENBQUMsQ0FBQztRQUNqQixJQUFJLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBQyxJQUFJLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sR0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUQsT0FBTyxHQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBQyxDQUFDLEVBQUMsT0FBTyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRCxNQUFNLENBQUMsSUFBSSxHQUFDLEdBQUcsR0FBQyxPQUFPLEdBQUMsR0FBRyxHQUFDLE9BQU8sQ0FBQztJQUN0QyxDQUFDO0lBQ0QsNkRBQTZEO0lBQzdELDhCQUFXLEdBQVgsVUFBWSxFQUFNLEVBQUcsTUFBVztRQUFoQyxpQkFvQkM7UUFuQkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFHLENBQUM7UUFDOUIsSUFBSSxJQUFJLEdBQUcsdUVBQXVFLENBQUM7UUFDN0UsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFDLFFBQVEsRUFBQyxFQUFFLEVBQUMsS0FBSyxFQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsQ0FBQyxDQUFDO1FBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQzthQUN6QixHQUFHLENBQUMsYUFBRyxJQUFJLFVBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBVixDQUFVLENBQUM7YUFDdEIsU0FBUyxDQUFDLGNBQUk7WUFDWiw2QkFBNkI7WUFDNUIsS0FBSSxDQUFDLEtBQUssR0FBQyxxRkFBcUYsQ0FBQztZQUNqRyxLQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxjQUFLLENBQUMsQ0FBQyxDQUFDO1lBQy9KLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixDQUFDLEVBQUUsZUFBSztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdCLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7SUFHYixDQUFDO0lBRUQsZ0NBQWEsR0FBYixVQUFjLEVBQUU7UUFBaEIsaUJBMENDO1FBekNDLElBQUksQ0FBQyxLQUFLLEdBQUMscUNBQXFDLEdBQUMsRUFBRSxHQUFDLEdBQUcsQ0FBQztRQUN4RCxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQyxFQUFDLFVBQUMsSUFBSTtZQUNyRCxLQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBQyxDQUFDLEdBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7Z0JBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckMsSUFBRyxDQUFDO29CQUNKLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUFBLENBQUM7Z0JBQ2pGLEtBQUssRUFBQyxHQUFHLENBQUMsRUFBQztvQkFDVCxLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO29CQUNqRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQzFCLENBQUM7Z0JBQ0QsSUFBSSxJQUFJLEdBQU8sSUFBSSxDQUFDO2dCQUNwQixJQUFJLElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3pELElBQUksR0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxTQUFTLEdBQUMsSUFBSSxDQUFDO2dCQUNwQixLQUFJLENBQUMsT0FBTyxHQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDeEMsSUFBSSxHQUFDLElBQUksQ0FBQztnQkFDVixJQUFJLElBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUMzRixJQUFJLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBQyxJQUFJLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QyxLQUFJLENBQUMsU0FBUyxJQUFHLEdBQUcsR0FBQyxJQUFJLEdBQUMsS0FBSyxDQUFDO2dCQUVoQyxLQUFJLENBQUMsWUFBWSxHQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxLQUFLLENBQUM7Z0JBQzNELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLFlBQVksR0FBRSxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3JELEtBQUksQ0FBQyxZQUFZLEdBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDeEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNwRCxLQUFJLENBQUMsVUFBVSxHQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO2dCQUV6QyxFQUFFLEVBQUMsQ0FBQyxJQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsRUFBQztvQkFFdEMsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBQyxDQUFDLEdBQUMsS0FBSSxDQUFDLFVBQVUsRUFBQyxDQUFDLEVBQUU7d0JBQ2pDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFNUIsQ0FBQztZQUNILENBQUM7WUFDRCxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3ZCLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUdELG9DQUFpQixHQUFqQixVQUFrQixFQUFNO1FBQ2xCLElBQUksSUFBSSxHQUFHLCtEQUErRCxDQUFDO1FBQzNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBQyxRQUFRLEVBQUMsRUFBRSxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsU0FBUyxFQUFDLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDO2FBQ3pCLEdBQUcsQ0FBQyxhQUFHLElBQUksVUFBRyxDQUFDLElBQUksRUFBRSxFQUFWLENBQVUsQ0FBQzthQUN0QixTQUFTLENBQUMsY0FBSTtZQUNmLDZCQUE2QjtZQUM5Qix1Q0FBdUM7UUFDdEMsQ0FBQyxFQUFFLGVBQUs7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCx5QkFBTSxHQUFOLFVBQU8sS0FBSztRQUFaLGlCQXVGRztRQXRGRCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUMsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsSUFBSSxXQUFXLEdBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUM1QixHQUFHLEVBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsVUFBVSxFQUFDLENBQUMsRUFBRSxFQUMvQixDQUFDO1lBQ0MsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFDLENBQUMsQ0FBQztZQUM1QyxFQUFFLEVBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUN2RCxDQUFDO2dCQUNDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBUyxHQUFHLEVBQUMsQ0FBQztvQkFDbEQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQztZQUNELFdBQVcsQ0FBQyxJQUFJLENBQUM7Z0JBQ2YsT0FBTyxFQUFDLENBQUM7Z0JBQ1IsUUFBUSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUN6QyxRQUFRLEVBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDbEYsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ25CLE9BQU8sRUFBQyxJQUFJLENBQUMsU0FBUztZQUN0QixVQUFVLEVBQUMsSUFBSSxDQUFDLFNBQVM7WUFDekIsUUFBUSxFQUFDLElBQUksQ0FBQyxPQUFPO1lBQ3JCLFlBQVksRUFBQyxXQUFXO1NBQ3pCLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUMxQyxJQUFJLElBQUksR0FBQyw2REFBNkQsQ0FBQztRQUN2RSxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBQztZQUMxQixJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hDLE9BQU8sRUFBQyxlQUFlO2dCQUN2QixZQUFZLEVBQUMsSUFBSTtnQkFDakIsT0FBTyxFQUFDLEtBQUs7YUFDZCxDQUFDLENBQUM7UUFDTCxDQUFDO1FBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUM7WUFFNUIsSUFBSSxDQUFDLE1BQU0sR0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDO2dCQUN4QyxPQUFPLEVBQUMsZUFBZTtnQkFDdkIsWUFBWSxFQUFDLElBQUk7Z0JBQ25CLE9BQU8sRUFBQyxNQUFNO2FBQ2YsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUNDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUM7WUFDM0IsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNuRCxTQUFTLENBQUMsY0FBSTtnQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUFBLEVBQUUsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUUsU0FBUyxDQUFDLEVBQUM7b0JBQy9DLEtBQUksQ0FBQyxLQUFLLEdBQUMsa0ZBQWtGLENBQUM7b0JBQzlGLEtBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFDO3dCQUM5RyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUN0QixFQUFFLEVBQUMsS0FBSyxDQUFDOzRCQUNULEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxDQUFDO2dCQUFBLENBQUM7Z0JBQ0osSUFBSSxFQUFDO29CQUNILEVBQUUsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUUsT0FBTyxDQUFDLEVBQUM7d0JBQ3pCLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLDRCQUE0QixDQUFDLENBQUM7d0JBQ25ELEtBQUksQ0FBQyxLQUFLLEdBQUMsa0ZBQWtGLENBQUM7d0JBQ2hHLEtBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxFQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsRUFBQyxJQUFJLENBQUMsQ0FBQyxFQUFDOzRCQUNyRyxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDOzRCQUN0QixLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDOzRCQUNsRSxFQUFFLEVBQUMsS0FBSyxDQUFDO2dDQUNKLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQ3JCLENBQUMsQ0FBQyxDQUFDO29CQUNMLENBQUM7b0JBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBRSxlQUFlLENBQUMsRUFBQzt3QkFDakMsS0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsNEJBQTRCLENBQUMsQ0FBQzt3QkFDbkQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDekIsRUFBRSxFQUFDLEtBQUssQ0FBQzs0QkFDTixLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUNyQixDQUFDO2dCQUNELENBQUM7WUFDSCxDQUFDLEVBQUUsZUFBSztnQkFDTixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN0QixLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO2dCQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDN0IsS0FBSSxDQUFDLEtBQUssR0FBQyxrRkFBa0YsQ0FBQztnQkFDOUYsS0FBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFDLENBQUMsS0FBSSxDQUFDLEtBQUssRUFBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxFQUFDLElBQUksQ0FBQyxDQUFDLEVBQUM7b0JBQ3ZHLEVBQUUsRUFBQyxLQUFLLENBQUM7d0JBQ1AsS0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQUVILG9CQUFvQjtJQUNwQixtQ0FBZ0IsR0FBaEI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QixFQUFFLEVBQUMsSUFBSSxDQUFDLE1BQU0sSUFBRSxTQUFTLENBQUM7WUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN2RCxFQUFFLEVBQUMsSUFBSSxDQUFDLE9BQU8sSUFBRSxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsQ0FBQyxFQUFDO1lBQ2xDLEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUM7Z0JBQzFELElBQUksQ0FBQyxNQUFNLEdBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNmLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqQixJQUFJLENBQUMsbUJBQW1CLEdBQUMsRUFBRSxDQUFDO1lBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDekUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDcEQsSUFBSSxDQUFDLE1BQU0sR0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEMsRUFBRSxFQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QixJQUFJLENBQUMsbUJBQW1CLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxDQUFDO1FBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLElBQUUsSUFBSSxDQUFDLFVBQVUsR0FBQyxDQUFDLENBQUMsQ0FDbkMsQ0FBQztZQUNDLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxFQUFFLENBQUM7WUFDbkQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUMzRCxDQUFDO0lBQ0QsQ0FBQztJQUNELDhCQUFXLEdBQVgsVUFBWSxLQUFLO1FBQ2YsRUFBRSxFQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdFLElBQUk7WUFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUNILDBCQUEwQjtJQUUxQiw0QkFBUyxHQUFULFVBQVUsR0FBRztRQUNkLDJDQUEyQztRQUN4QyxFQUFFLEVBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBRSxDQUFDLENBQUMsQ0FBQztZQUMzQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsSUFBSTtZQUNGLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGtDQUFlLEdBQWY7UUFDRSxFQUFFLEVBQUMsSUFBSSxDQUFDLE9BQU8sSUFBRSxDQUFDLENBQUM7WUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsbUNBQW1DLENBQUMsQ0FBQztRQUM5RCxFQUFFLEVBQUMsSUFBSSxDQUFDLE9BQU8sSUFBRSxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsQ0FBQyxDQUNqQyxDQUFDO1lBQ0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN6QyxDQUFDO1FBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLEdBQUMsQ0FBQyxDQUFDLEVBQUM7WUFDakIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLEVBQUUsQ0FBQztZQUNyRCxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDZixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUN6RSxJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLENBQUMsbUJBQW1CLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUN0QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN4RCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN0RCxDQUFDO0lBRUQsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDO0FBNVdZLFFBQVE7SUFMcEIsd0VBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxXQUFXO09BQ0c7S0FDekIsQ0FBQzthQW1DK087QUEyVWhQO1NBNVdZLFFBQVEsZTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2QjJCO0FBQ0s7QUFHckQsSUFBYSxZQUFZO0lBQ3pCLHNCQUFvQixTQUEwQjtRQUExQixjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFDRCxnQ0FBUyxHQUFULFVBQVUsT0FBTztRQUNiLElBQUksS0FBSyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1lBQzVCLE9BQU8sRUFBQyxPQUFPO1lBQ2YsUUFBUSxFQUFDLElBQUk7WUFDYixRQUFRLEVBQUMsUUFBUTtTQUNwQixDQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsWUFBWSxDQUFDO1lBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNuQixLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUNELG1CQUFDO0FBQUQsQ0FBQztBQWhCWSxZQUFZO0lBRHhCLHlFQUFVLEVBQUU7cUNBRWtCLHNFQUFlO0dBRGpDLFlBQVksQ0FnQnhCO0FBaEJ3Qjs7Ozs7Ozs7QUNKekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLDRDQUE0QyxXQUFXO0FBQ3ZEO0FBQ0E7QUFDQSxrQzs7Ozs7OztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUIwQztBQUVDO0FBQ007QUFDVDtBQUt4QyxJQUFhLFFBQVE7SUFNbkI7UUFKQSxhQUFRLEdBQUcsNERBQVEsQ0FBQztRQUNwQixhQUFRLEdBQUcsK0RBQVMsQ0FBQztRQUNyQixhQUFRLEdBQUcscUVBQVcsQ0FBQztJQUl2QixDQUFDO0lBQ0gsZUFBQztBQUFELENBQUM7QUFUWSxRQUFRO0lBSHBCLHdFQUFTLENBQUM7T0FDZTtLQUN6QixDQUFDOztBQVVEO1NBVFksUUFBUSxlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1RxQjtBQUNJO0FBTTlDLElBQWEsU0FBUztJQUVwQixtQkFBbUIsT0FBc0I7UUFBdEIsWUFBTyxHQUFQLE9BQU8sQ0FBZTtJQUV6QyxDQUFDO0lBRUgsZ0JBQUM7QUFBRCxDQUFDO0FBTlksU0FBUztJQUpyQix3RUFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFlBQVk7T0FDRztLQUMxQixDQUFDO2NBR3lDO0FBSTFDO1NBTlksU0FBUyxlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNQb0I7QUFDSTtBQUNlO0FBSzdELElBQWEsV0FBVztJQUV0QixxQkFBbUIsS0FBbUIsRUFBUSxPQUFzQjtRQUFwRSxpQkFNQztRQU5rQixVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQVEsWUFBTyxHQUFQLE9BQU8sQ0FBZTtRQUR0RSxtQkFBYyxHQUFTLEtBQUssQ0FBQztRQUV6QixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFDLFNBQWlCO1lBQzlDLEVBQUUsRUFBQyxTQUFTLENBQUMsRUFBQztnQkFDWixLQUFJLENBQUMsY0FBYyxHQUFDLFNBQVMsQ0FBQztZQUNoQyxDQUFDO1FBQ0gsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELCtCQUFTLEdBQVQ7UUFDRSxJQUFJLFNBQVMsR0FBQztZQUNaLEVBQUUsRUFBQyw4QkFBOEI7WUFDakMsT0FBTyxFQUFDLE9BQU87WUFDZixJQUFJLEVBQUMsRUFBRTtZQUNQLE1BQU0sRUFBQyxJQUFJO1NBQ1osQ0FBQztRQUNKLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFDSCxrQkFBQztBQUFELENBQUM7QUFsQlksV0FBVztJQUp2Qix3RUFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGNBQWM7T0FDRztLQUM1QixDQUFDO2dCQUdvRTtBQWdCckU7U0FsQlksV0FBVyxlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BpQjtBQUNTO0FBQ29CO0FBQ3BCO0FBQ2I7QUFDTjtBQUVvQztBQUNuQjtBQUVQO0FBS3pDLElBQWEsUUFBUTtJQU1uQixrQkFBbUIsUUFBaUIsRUFBUyxJQUFTLEVBQVEsaUJBQW1DLEVBQVMsT0FBcUIsRUFBUSxRQUF5QjtRQUE3SSxhQUFRLEdBQVIsUUFBUSxDQUFTO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBSztRQUFRLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUFjO1FBQVEsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFGaEssb0JBQWUsR0FBTyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBR2hDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsRUFBRSxFQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVFLGlGQUFpRjtJQUVqRixDQUFDO0lBQ0QsbUNBQWdCLEdBQWhCO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ25DLEVBQUUsRUFBQyxPQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFFLFdBQVcsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFFLE9BQU8sSUFBRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFFLElBQUssQ0FBQyxFQUFDO1lBQzNKLGtFQUFrRTtZQUNsRSx5Q0FBeUM7WUFDekMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMscUVBQVMsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNqQyxDQUFDO0lBQ0gsQ0FBQztJQUVELHVCQUFJLEdBQUo7UUFBQSxpQkFzQkM7UUFyQkMsRUFBRSxFQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFFLElBQUksQ0FBQztZQUNoRCxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUMsRUFBRSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGVBQWUsR0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBQ2pDLElBQUksSUFBSSxHQUFDLHlEQUF5RCxDQUFDO1FBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUMsS0FBSyxFQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUMsQ0FBQyxDQUFDO2FBQzVFLEdBQUcsQ0FBQyxhQUFHLElBQUUsVUFBRyxDQUFDLElBQUksRUFBRSxFQUFWLENBQVUsQ0FBQzthQUNwQixTQUFTLENBQUMsY0FBSTtZQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsS0FBSSxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUM7WUFDbkIsRUFBRSxFQUFDLE9BQU0sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUUsV0FBVyxDQUFDLENBQ3BDLENBQUM7Z0JBQ0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDeEIsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBQyxDQUFDLEdBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFDLENBQUMsRUFBRSxFQUFDLENBQUM7b0JBQ2xELEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxHQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBQyxNQUFNO29CQUN0RyxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRCxDQUFDO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7UUFDSCxDQUFDLEVBQUMsZUFBSztZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUNqRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx5QkFBTSxHQUFOLFVBQU8sU0FBUztRQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkIsRUFBRSxFQUFDLE9BQU0sQ0FBQyxTQUFTLENBQUMsSUFBRSxXQUFXLElBQUksU0FBUyxJQUFFLElBQUksQ0FBQztZQUNuRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyx5RkFBZSxFQUFDO2dCQUNoQyxXQUFXLEVBQUMsU0FBUzthQUN0QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0NBQWUsR0FBZjtRQUFBLGlCQXlCQztRQXhCQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBQztZQUMxQixJQUFJLENBQUMsTUFBTSxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7Z0JBQ3hDLE9BQU8sRUFBQyxlQUFlO2dCQUN2QixZQUFZLEVBQUMsSUFBSTtnQkFDakIsT0FBTyxFQUFDLEtBQUs7YUFDZCxDQUFDLENBQUM7UUFDTCxDQUFDO1FBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUM7WUFDOUIsSUFBSSxDQUFDLE1BQU0sR0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDO2dCQUN4QyxPQUFPLEVBQUMsZUFBZTtnQkFDdkIsWUFBWSxFQUFDLElBQUk7Z0JBQ2pCLE9BQU8sRUFBQyxNQUFNO2FBQ2YsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQzFDLEVBQUUsRUFBQyxPQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFFLFdBQVcsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFFLE9BQU8sSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFFLElBQUssQ0FBQyxFQUFDO1lBQzVKLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDO2dCQUN6QixLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDZCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxTQUFTO2dCQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7SUFDSCxDQUFDO0lBRUgsZUFBQztBQUFELENBQUM7QUFqRlksUUFBUTtJQUpwQix3RUFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFdBQVc7T0FDRztLQUN6QixDQUFDO2FBT2dLO0FBMkVqSztTQWpGWSxRQUFRLGU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZnNCO0FBQ047QUFDTjtBQUNzQztBQUVyRSxJQUFhLGdCQUFnQjtJQUczQiwwQkFBbUIsSUFBVSxFQUFRLE1BQXVCO1FBQXpDLFNBQUksR0FBSixJQUFJLENBQU07UUFBUSxXQUFNLEdBQU4sTUFBTSxDQUFpQjtRQUMxRCxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUNELG1EQUF3QixHQUF4QjtRQUFBLGlCQXlCQztRQXhCQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxpREFBaUQsRUFBQyxFQUFFLENBQUMsRUFBQyxVQUFDLElBQUk7WUFDOUYsS0FBSSxDQUFDLFVBQVUsR0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLGNBQUk7b0JBQ3pGLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xCLEVBQUUsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUUsU0FBUyxDQUFDLEVBQUM7d0JBQzdCLEtBQUksQ0FBQyxLQUFLLEdBQUMsa0ZBQWtGLENBQUM7d0JBQzlGLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFDO3dCQUN6SixDQUFDLENBQUMsQ0FBQztvQkFBQSxDQUFDO29CQUNKLElBQUksRUFBQzt3QkFBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUV0QixLQUFJLENBQUMsS0FBSyxHQUFDLGtGQUFrRixDQUFDO3dCQUM5RixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUMsSUFBSSxDQUFDLENBQUMsRUFBQzt3QkFDaEosQ0FBQyxDQUFDLENBQUM7b0JBQ0wsQ0FBQztnQkFDSCxDQUFDLEVBQUMsZUFBSztvQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUMzQixLQUFJLENBQUMsS0FBSyxHQUFDLGtGQUFrRixDQUFDO29CQUM5RixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUMsSUFBSSxDQUFDLENBQUMsRUFBQztvQkFDaEosQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0gsdUJBQUM7QUFBRCxDQUFDO0FBaENZLGdCQUFnQjtJQUQ1Qix5RUFBVSxFQUFFO3FDQUljLDJEQUFJLEVBQWUsd0ZBQWdCO0dBSGpELGdCQUFnQixDQWdDNUI7QUFoQzRCOzs7Ozs7Ozs7Ozs7QUNMOEM7QUFFbEM7QUFFekMseUdBQXNCLEVBQUUsQ0FBQyxlQUFlLENBQUMsOERBQVMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0pHO0FBQ0c7QUFDZTtBQUNqQztBQUNHO0FBQ007QUFDTTtBQUNUO0FBQ0E7QUFDQTtBQUNPO0FBQ007QUFDTztBQUNFO0FBQ3ZCO0FBQ1k7QUFDRztBQUNaO0FBQ2dCO0FBQ2Y7QUFDb0I7QUFDUjtBQWlEN0QsSUFBYSxTQUFTO0lBQXRCO0lBQXdCLENBQUM7SUFBRCxnQkFBQztBQUFELENBQUM7QUFBWixTQUFTO0lBaERyQix1RUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osNkRBQUs7WUFDTCxxRUFBUztZQUNULDJFQUFXO1lBQ1gsa0VBQVE7WUFDUiwwRkFBZTtZQUNmLGtFQUFRO1lBQ1Isa0VBQVE7WUFDUixzRUFBUztTQUNWO1FBQ0QsT0FBTyxFQUFFO1lBQ1AsZ0ZBQWE7WUFDYixpRUFBVTtZQUNWLGtFQUFXLENBQUMsT0FBTyxDQUFDLDZEQUFLLEVBQUUsRUFBRSxFQUNqQztnQkFDRSxLQUFLLEVBQUU7b0JBQ0wsRUFBRSxZQUFZLEVBQUUsaUVBQWlFLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFO29CQUMxSyxFQUFFLFlBQVksRUFBRSw2Q0FBNkMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFO29CQUN6SSxFQUFFLFlBQVksRUFBRSwwQ0FBMEMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsRUFBRSxFQUFFO2lCQUNySTthQUNGLENBQUM7U0FDQztRQUNELFNBQVMsRUFBRSxDQUFDLCtEQUFRLENBQUM7UUFDckIsZUFBZSxFQUFFO1lBQ2YsNkRBQUs7WUFDTCxxRUFBUztZQUNULDJFQUFXO1lBQ1gsc0VBQVM7WUFDVCxrRUFBUTtZQUNSLDBGQUFlO1lBQ2Ysa0VBQVE7WUFDUixrRUFBUTtTQUNUO1FBQ0QsU0FBUyxFQUFFO1lBQ1QsdUZBQWM7WUFDZCxnRkFBWTtZQUNaLDRFQUFTO1lBQ1Qsa0ZBQVk7WUFDWixFQUFDLE9BQU8sRUFBRSxtRUFBWSxFQUFFLFFBQVEsRUFBRSx3RUFBaUIsRUFBQztZQUNwRCx1RkFBZ0I7WUFDaEIseUZBQWdCO1lBQ2hCLG1GQUFjO1lBQ2Qsb0ZBQWE7WUFDYixxRUFBTTtZQUNOLHVFQUFPO1NBQ1I7S0FDRixDQUFDO0dBQ1csU0FBUyxDQUFHO0FBQUg7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RFb0I7QUFFRDtBQUNZO0FBQ007QUFFYjtBQUs5QyxJQUFhLEtBQUs7SUFHaEIsZUFBWSxRQUFrQixFQUFFLFNBQW9CLEVBQUUsWUFBMEI7UUFGaEYsYUFBUSxHQUFPLGtFQUFRLENBQUM7UUFHdEIsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQztZQUNwQixnRUFBZ0U7WUFDaEUsaUVBQWlFO1lBQ2pFLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUN6QixZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0gsWUFBQztBQUFELENBQUM7QUFYWSxLQUFLO0lBSGpCLHdFQUFTLENBQUM7T0FDYztLQUN4QixDQUFDO1VBSWdGO0FBUWpGO1NBWFksS0FBSywyQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1h5QjtBQUVpQjtBQUM3QjtBQUUvQjs7Ozs7RUFLRTtBQUVGLElBQWEsZ0JBQWdCO0lBRzNCLDBCQUFvQixNQUFhO1FBQWIsV0FBTSxHQUFOLE1BQU0sQ0FBTztJQUVqQyxDQUFDO0lBQ0QsbUNBQVEsR0FBUixVQUFTLE1BQU0sRUFBQyxLQUFLLEVBQUMsUUFBUTtRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUNmLElBQUksRUFBQyxNQUFNLEdBQUMsS0FBSztZQUNqQixRQUFRLEVBQUMsU0FBUztTQUNyQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsRUFBZTtZQUV0QixFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBQyxJQUFJLENBQUMsQ0FBQyxTQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBQyxDQUFDO2lCQUN4RyxLQUFLLENBQUMsV0FBQyxJQUFFLGNBQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQWQsQ0FBYyxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQUMsSUFBRSxjQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFkLENBQWMsQ0FBQyxDQUFDO0lBRWxDLENBQUM7SUFDRCx1QkFBQztBQUFELENBQUM7QUFqQlksZ0JBQWdCO0lBRDVCLHlFQUFVLEVBQUU7cUNBSWdCLG9FQUFNO0dBSHRCLGdCQUFnQixDQWlCNUI7QUFqQjRCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaeUM7QUFDcEI7QUFHbEQsSUFBYSxjQUFjO0lBRXZCLHdCQUFtQixPQUFlO1FBQWYsWUFBTyxHQUFQLE9BQU8sQ0FBUTtJQUVsQyxDQUFDO0lBQ0Qsc0NBQWEsR0FBYixVQUFjLE9BQU8sRUFBQyxLQUFLLEVBQUMsV0FBVyxFQUFDLElBQUksRUFBQyxRQUFRO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBQyxLQUFLLEVBQUMsV0FBVyxFQUFDLElBQUksQ0FBQzthQUNsRCxJQUFJLENBQUMsVUFBQyxJQUFJLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxJQUFJLENBQUMsQ0FBQyxTQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQzthQUMxRSxLQUFLLENBQUMsVUFBQyxHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBQyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUNELHVDQUFjLEdBQWQsVUFBZSxPQUFjLEVBQUMsS0FBWSxFQUFDLFdBQW9CLEVBQUMsUUFBWTtRQUN4RSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUMsS0FBSyxFQUFDLFdBQVcsQ0FBQzthQUM5QyxJQUFJLENBQUMsVUFBQyxhQUFhO1lBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQyxHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxHQUFDLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQUFDO0FBbEJZLGNBQWM7SUFEMUIseUVBQVUsRUFBRTtxQ0FHa0Isc0VBQU87R0FGekIsY0FBYyxDQWtCMUI7QUFsQjBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKZTtBQUNvQjtBQUN6QjtBQUMyQjtBQUVEO0FBRS9EOzs7OztFQUtFO0FBT0YsSUFBYSxTQUFTO0lBVXBCLG1CQUFtQixNQUFxQixFQUFRLEdBQU8sRUFBUSxRQUF1QixFQUFRLElBQVMsRUFBUyxPQUFzQixFQUFTLFNBQW9CO1FBQWhKLFdBQU0sR0FBTixNQUFNLENBQWU7UUFBUSxRQUFHLEdBQUgsR0FBRyxDQUFJO1FBQVEsYUFBUSxHQUFSLFFBQVEsQ0FBZTtRQUFRLFNBQUksR0FBSixJQUFJLENBQUs7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQU5uSyxzQkFBaUIsR0FBUyxJQUFJLENBQUM7UUFDL0IsZ0JBQVcsR0FBUyxJQUFJLENBQUM7UUFDekIsbUJBQWMsR0FBUyxLQUFLLENBQUM7UUFHN0IsaUJBQVksR0FBVSxDQUFDLGVBQWUsRUFBQyxnQkFBZ0IsRUFBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBRTVFLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7SUFFN0IsQ0FBQztJQUVELHFDQUFpQixHQUFqQixVQUFrQixHQUFHO1FBQXJCLGlCQThEQztRQTdEQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksSUFBSSxHQUFDLGdFQUFnRSxDQUFDO1FBQzFFLElBQUksT0FBTyxHQUFHO1lBQ1osS0FBSyxFQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO1lBQ3RDLE9BQU8sRUFBQyxFQUFFO1lBQ1YsUUFBUSxFQUFDLEVBQUU7WUFDWCxlQUFlLEVBQUMsQ0FBQztZQUNqQixVQUFVLEVBQUMsSUFBSSxDQUFDLFVBQVU7U0FBQyxDQUFDO1FBQzlCLE1BQU0sRUFBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBQztZQUNsQixLQUFLLENBQUM7Z0JBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxXQUFXLElBQUUsSUFBSSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBRSxJQUFJLENBQUMsRUFBQztvQkFDaEUsT0FBTyxDQUFDLE9BQU8sR0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUM5QixPQUFPLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQ3BDLENBQUM7Z0JBQ0QsSUFBSTtvQkFDRixNQUFNLENBQUM7Z0JBQ1AsS0FBSyxDQUFDO1lBQ1IsS0FBSyxDQUFDO2dCQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsaUJBQWlCLElBQUUsSUFBSSxDQUFDLEVBQUM7b0JBQ3RDLE9BQU8sQ0FBQyxPQUFPLEdBQUMsSUFBSSxDQUFDLFdBQVcsR0FBQyxHQUFHLEdBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztvQkFDckQsT0FBTyxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUNwQyxDQUFDO2dCQUNELElBQUk7b0JBQ0YsTUFBTSxDQUFDO2dCQUNULEtBQUssQ0FBQztZQUNOLEtBQUssQ0FBQztnQkFBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFFLElBQUksQ0FBQyxFQUFDO29CQUN0QyxPQUFPLENBQUMsT0FBTyxHQUFDLElBQUksQ0FBQyxRQUFRLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQ2xELE9BQU8sQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDcEMsQ0FBQztnQkFDRCxJQUFJO29CQUNGLE1BQU0sQ0FBQztnQkFDVCxLQUFLLENBQUM7WUFDTixLQUFLLENBQUM7Z0JBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBRSxJQUFJLENBQUMsRUFBQztvQkFDdEMsT0FBTyxDQUFDLE9BQU8sR0FBQyxJQUFJLENBQUMsUUFBUSxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUNsRCxPQUFPLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQ3BDLENBQUM7Z0JBQ0QsSUFBSTtvQkFDRixNQUFNLENBQUM7Z0JBQ1QsS0FBSyxDQUFDO1FBQ1IsQ0FBQztRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFO2FBQ2xCLElBQUksQ0FBQyxVQUFDLEVBQU07WUFDWCxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxPQUFPLENBQUMsS0FBSyxHQUFDLEVBQUUsQ0FBQztZQUNqQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDckIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQztpQkFDekIsU0FBUyxDQUFDLGNBQUk7Z0JBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsRUFBRSxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBRSxTQUFTLENBQUMsRUFBQztvQkFDM0IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUMsTUFBTSxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLENBQUM7Z0JBQ0QsS0FBSSxDQUFDLFFBQVEsR0FBQyxFQUFFLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxXQUFXLEdBQUMsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxlQUFLO2dCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBQyxPQUFPLENBQUMsQ0FBQztnQkFDL0MsS0FBSSxDQUFDLFFBQVEsR0FBQyxFQUFFLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxXQUFXLEdBQUMsRUFBRSxDQUFDO1lBQ3BCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDO2FBQ0MsS0FBSyxDQUFDLFVBQUMsR0FBRyxJQUFHLGNBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsRUFBckMsQ0FBcUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxzQ0FBa0IsR0FBbEIsVUFBbUIsS0FBSztRQUF4QixpQkF3QkM7UUF2QkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixNQUFNLEVBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUM7WUFDcEIsS0FBSyxDQUFDO2dCQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7Z0JBQ25FLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxFQUFFLENBQUM7Z0JBQ25ELEtBQUssQ0FBQztZQUNOLEtBQUssQ0FBQztnQkFDTixJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsRUFBQyxTQUFTLEVBQUMsQ0FBQyxLQUFLLEVBQUMsSUFBSSxDQUFDLEVBQUMsVUFBQyxPQUFPO29CQUNwRixNQUFNLEVBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUM7d0JBQ3RCLEtBQUssQ0FBQzs0QkFDTixLQUFLLENBQUM7d0JBQ04sS0FBSyxDQUFDOzRCQUNOLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs0QkFDdEMsS0FBSyxDQUFDO3dCQUNOLEtBQUssQ0FBQzs0QkFDTixLQUFLLENBQUM7b0JBQ1IsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxDQUFDO2dCQUNHLEtBQUssQ0FBQztZQUNaLEtBQUssQ0FBQztnQkFBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO2dCQUNuRSxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsRUFBRSxDQUFDO2dCQUN2RCxLQUFLLENBQUM7UUFDUixDQUFDO0lBQ0gsQ0FBQztJQUNELGtDQUFjLEdBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDeEMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUN2RCxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1FBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7UUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1FBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7UUFDL0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztJQUM1QyxDQUFDO0lBRUQsMEJBQU0sR0FBTixVQUFPLEdBQUc7UUFBVixpQkE0REM7UUExREcsTUFBTSxFQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFDO1lBQ2xCLEtBQUssQ0FBQyxDQUFDO1lBQ1AsS0FBSyxDQUFDO2dCQUFLLEVBQUUsRUFBQyxJQUFJLENBQUMsV0FBVyxJQUFFLElBQUksSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUUsSUFBSSxDQUFDLENBQ3JFLENBQUM7b0JBQ0QsSUFBSSxJQUFJLEdBQUcsZ0VBQWdFLENBQUM7b0JBQ2hGLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFO3lCQUNoQixJQUFJLENBQUMsVUFBQyxFQUFNO3dCQUNYLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNwQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDOzRCQUN4QixLQUFLLEVBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7NEJBQ3RDLE9BQU8sRUFBQyxLQUFJLENBQUMsUUFBUTs0QkFDckIsUUFBUSxFQUFDLEtBQUksQ0FBQyxXQUFXOzRCQUN6QixlQUFlLEVBQUMsQ0FBQzs0QkFDakIsVUFBVSxFQUFDLENBQUM7eUJBQUMsQ0FBQyxDQUFDO3dCQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3BCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUM7NkJBQ3pCLFNBQVMsQ0FBQyxjQUFJOzRCQUNiLEVBQUUsRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUUsU0FBUyxDQUFDLEVBQUM7Z0NBQzNCLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDM0IsS0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQzs0QkFDckIsQ0FBQzs0QkFDRCxLQUFJLENBQUMsUUFBUSxHQUFDLEVBQUUsQ0FBQzs0QkFDakIsS0FBSSxDQUFDLFdBQVcsR0FBQyxFQUFFLENBQUM7d0JBQ3RCLENBQUMsRUFBRSxlQUFLOzRCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUM3QixZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBQyxPQUFPLENBQUMsQ0FBQzs0QkFDL0MsS0FBSSxDQUFDLFFBQVEsR0FBQyxFQUFFLENBQUM7NEJBQ2pCLEtBQUksQ0FBQyxXQUFXLEdBQUMsRUFBRSxDQUFDO3dCQUNwQixDQUFDLENBQUMsQ0FBQztvQkFDUCxDQUFDLENBQUM7eUJBQ0MsS0FBSyxDQUFDLFVBQUMsR0FBRyxJQUFHLGNBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsRUFBckMsQ0FBcUMsQ0FBQyxDQUFDO2dCQUN2RCxDQUFDO2dCQUNHLEtBQUssQ0FBQztZQUNOLEtBQUssQ0FBQztnQkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3RDLEVBQUUsRUFBQyxJQUFJLENBQUMsUUFBUSxJQUFFLEdBQUcsSUFBRyxPQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFFLElBQUksQ0FBQyxFQUFDO29CQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUM1QixJQUFJLElBQUksR0FBRyxtRUFBbUUsQ0FBQztvQkFDakYsSUFBSSxDQUFDLFVBQVUsR0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUM1QixJQUFJLElBQUksR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO3dCQUN4QixTQUFTLEVBQUMsSUFBSSxDQUFDLFFBQVE7cUJBQ3hCLENBQUMsQ0FBQztvQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDO3lCQUN6QixTQUFTLENBQUMsY0FBSTt3QkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ3JELEtBQUksQ0FBQyxRQUFRLEdBQUMsRUFBRSxDQUFDO3dCQUNqQixLQUFJLENBQUMsV0FBVyxHQUFDLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxFQUFFLGVBQUs7d0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzdCLEtBQUksQ0FBQyxRQUFRLEdBQUMsRUFBRSxDQUFDO3dCQUNqQixLQUFJLENBQUMsV0FBVyxHQUFDLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxDQUFDLENBQUM7Z0JBQUEsQ0FBQztnQkFDSixLQUFLLENBQUM7UUFDUixDQUFDO0lBR0wsQ0FBQztJQUNELGlDQUFhLEdBQWIsVUFBYyxHQUFHO1FBQ2YsTUFBTSxFQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFDO1lBQ2xCLEtBQUssQ0FBQztnQkFBRSxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDLENBQzFFLENBQUM7b0JBQ0csUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFDLEtBQUssQ0FBQztvQkFDL0QsSUFBSSxDQUFDLFdBQVcsR0FBQyxJQUFJLENBQUM7Z0JBQ3BCLENBQUM7Z0JBQ0MsSUFBSSxFQUFDO29CQUNMLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBQyxPQUFPLENBQUM7b0JBQy9ELElBQUksQ0FBQyxXQUFXLEdBQUMsS0FBSyxDQUFDO2dCQUN2QixDQUFDO2dCQUNDLEtBQUssQ0FBQztZQUNaLEtBQUssQ0FBQztnQkFBQyxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUNqRCxDQUFDO29CQUNDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxJQUFJLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBQyxLQUFLLENBQUM7Z0JBQ2hFLENBQUM7Z0JBQ0csSUFBSSxFQUFDO29CQUNMLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBQyxPQUFPLENBQUM7b0JBQzVELElBQUksQ0FBQyxpQkFBaUIsR0FBQyxLQUFLLENBQUM7Z0JBQ2pDLENBQUM7Z0JBQ0MsS0FBSyxDQUFDO1FBQ1osQ0FBQztJQUNILENBQUM7SUFDRCxrQ0FBYyxHQUFkLFVBQWUsR0FBRztRQUNqQixFQUFFLEVBQUMsR0FBRyxJQUFFLGFBQWEsQ0FBQyxFQUFDO1lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUN2RCxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsRUFBRSxDQUFDO1lBQ3hELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsSUFBSSxDQUFDLGNBQWMsR0FBQyxJQUFJLENBQUM7UUFDM0IsQ0FBQztRQUFJLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFFLGlCQUFpQixDQUFDLEVBQUM7WUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMxQixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxFQUFFLENBQUM7WUFDeEQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxJQUFJLENBQUMsY0FBYyxHQUFDLElBQUksQ0FBQztRQUMzQixDQUFDO1FBQUEsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUUsZ0JBQWdCLENBQUMsRUFBQztZQUM5QixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDNUQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsRUFBRSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUNqRSxDQUFDO1FBQ0QsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUUsZ0JBQWdCLENBQUMsRUFBQztZQUM3QixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDNUQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLEVBQUUsQ0FBQztZQUMzRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUNqRSxDQUFDO1FBRUQsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUUsZ0JBQWdCLENBQUMsRUFBQztZQUM3QixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDNUQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxFQUFFLENBQUM7WUFDM0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUNqRSxDQUFDO1FBRUQsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUUsZ0JBQWdCLENBQUMsRUFBQztZQUM3QixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDNUQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsRUFBRSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztRQUNqRSxDQUFDO1FBRUQsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUUsZ0JBQWdCLENBQUMsRUFBQztZQUM3QixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQzNELFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDNUQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLE1BQU0sQ0FBQztZQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxNQUFNLENBQUM7WUFDL0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsTUFBTSxDQUFDO1lBQy9ELFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLEVBQUUsQ0FBQztRQUM3RCxDQUFDO0lBQ0gsQ0FBQztJQUNILGdCQUFDO0FBQUQsQ0FBQztBQWhSWSxTQUFTO0lBSnJCLHdFQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsWUFBWTtPQUNHO0tBQzFCLENBQUM7Y0FXbUs7QUFzUXBLO1NBaFJZLFNBQVMsZSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJb25pY1BhZ2UsIE5hdkNvbnRyb2xsZXIsIE5hdlBhcmFtcyB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgSHR0cCB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgVGVzdFBhZ2UgfSBmcm9tICcuLi90ZXN0L3Rlc3QnO1xuaW1wb3J0IHsgTG9naW5QYWdlIH0gZnJvbSAnLi4vbG9naW4vbG9naW4nO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9tYXAnO1xuaW1wb3J0IHtTcWxpdGVEYlByb3ZpZGVyfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc3FsaXRlLWRiL3NxbGl0ZS1kYic7XG5pbXBvcnQge0RpYWxvZ1Byb3ZpZGVyfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc2VydmljZXMvZGlhbG9nJztcbmltcG9ydCB7TG9hZGluZ0NvbnRyb2xsZXJ9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgUGxhdGZvcm0gfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbi8qKlxuICogR2VuZXJhdGVkIGNsYXNzIGZvciB0aGUgUHJvZmlsZUxpc3RQYWdlIHBhZ2UuXG4gKlxuICogU2VlIGh0dHA6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy9jb21wb25lbnRzLyNuYXZpZ2F0aW9uIGZvciBtb3JlIGluZm9cbiAqIG9uIElvbmljIHBhZ2VzIGFuZCBuYXZpZ2F0aW9uLlxuICovXG5cbkBJb25pY1BhZ2UoKVxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1wcm9maWxlLWxpc3QnLFxuICB0ZW1wbGF0ZVVybDogJ3Byb2ZpbGUtbGlzdC5odG1sJyxcbn0pXG5leHBvcnQgY2xhc3MgUHJvZmlsZUxpc3RQYWdlIHtcblxuICBxdWVyeTphbnk7XG4gIHBvc3RzOiBhbnk7XG4gIHRlbXBwb3N0OmFueTtcbiAgc3RvcmVkdGVzdDphbnk9W107XG4gIGludGVydmFsOmFueTsgXG4gIGxvYWRJbnRlcnZhbDphbnk7XG4gIHVzZXJfaWQ6YW55PW51bGw7XG4gIGxvYWRlcjphbnk9bnVsbDtcbiAgdGFiQmFyRWxlbWVudDphbnk7XG4gIGNvbXBhbnlJZDphbnk7XG4gIHRlc3RUeXBlOnN0cmluZ1tdPVtcIlRcIixcIkVcIixcIllcIixcIlNcIixcIlZcIixcIkZcIl07XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBuYXZQYXJhbXM6TmF2UGFyYW1zLHB1YmxpYyBsb2FkaW5nQ29udHJvbGxlcjpMb2FkaW5nQ29udHJvbGxlcixwdWJsaWMgcGxhdGZvcm06UGxhdGZvcm0sIHB1YmxpYyBzcWw6U3FsaXRlRGJQcm92aWRlcixwdWJsaWMgbmF2Q3RybDogTmF2Q29udHJvbGxlciwgcHVibGljIGh0dHA6IEh0dHAscHVibGljIGRpYWxvZzpEaWFsb2dQcm92aWRlcikge1xuICAgIHRoaXMuY29tcGFueUlkID0gdGhpcy5uYXZQYXJhbXMuZ2V0KCdjb21wYW55SWQnKTtcbiAgICBjb25zb2xlLmxvZyhcImNvbWFwbnkgSWRcIix0aGlzLmNvbXBhbnlJZCk7XG4gIH1cbiAgaW9uVmlld0RpZEVudGVyKCl7XG4gICAgaWYodGhpcy5wbGF0Zm9ybS5pcygnaW9zJykpe1xuICAgICAgdGhpcy5sb2FkZXI9dGhpcy5sb2FkaW5nQ29udHJvbGxlci5jcmVhdGUoe1xuICAgICAgICBjb250ZW50OlwiUGxlYXNlIFdhaXQuLlwiLFxuICAgICAgICBzaG93QmFja2Ryb3A6dHJ1ZSxcbiAgICAgICAgc3Bpbm5lcjonaW9zJ1xuICAgICAgfSk7XG4gICAgfVxuICAgIGlmKHRoaXMucGxhdGZvcm0uaXMoJ2FuZHJvaWQnKSl7XG4gICAgICB0aGlzLmxvYWRlcj10aGlzLmxvYWRpbmdDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICAgIGNvbnRlbnQ6XCJQbGVhc2UgV2FpdC4uXCIsXG4gICAgICAgIHNob3dCYWNrZHJvcDp0cnVlLFxuICAgICAgICBzcGlubmVyOidkb3RzJ1xuICAgICAgfSk7XG4gICAgfVxuICAgIHRoaXMudGFiQmFyRWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy50YWJiYXIuc2hvdy10YWJiYXInKTtcbiAgICB0aGlzLnRhYkJhckVsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICdmbGV4JztcbiAgICBjb25zb2xlLmxvZyhcImlvblZpZXdEaWRFbnRlclwiKTtcbiAgICBpZih0eXBlb2YobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIikpIT1cInVuZGVmaW5lZFwiICYmIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpIT1cImZhbHNlXCIgJiYgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIikhPW51bGwgKXtcbiAgICAgIHRoaXMubG9hZGVyLnByZXNlbnQoKS50aGVuKCgpPT57XG4gICAgICAgIHRoaXMubG9hZCgpOyAgICAgIFxuICAgICAgfSkuY2F0Y2goKGVycmxvYWRlcik9PntcbiAgICAgICAgY29uc29sZS5sb2coZXJybG9hZGVyLmNvZGUpO1xuICAgICAgfSk7ICAgIFxuICAgIH1cbiAgfVxuXG4gIGlvblZpZXdXaWxsRW50ZXIoKXtcbiAgICBpZih0eXBlb2YobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIikpPT1cInVuZGVmaW5lZFwiIHx8IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpPT1cImZhbHNlXCJ8fGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpPT1udWxsICl7XG4gICAgIC8vIHRoaXMubmF2Q3RybC5wdXNoKExvZ2luUGFnZSx7fSwoKT0+e2NvbnNvbGUubG9nKFwiZGlzcGxheWVkXCIpfSk7XG4gICAgIC8vIHRoaXMuYXBwLmdldFJvb3ROYXYoKS5wdXNoKExvZ2luUGFnZSk7XG4gICAgIHRoaXMubmF2Q3RybC5wdXNoKExvZ2luUGFnZSk7XG4gICAgIGNvbnNvbGUubG9nKFwid2lsbEVudGVyIGNhbGxlZFwiKTsgXG4gICAgfVxuICAgIGNvbnNvbGUubG9nKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpKTsgXG4gIH1cbmxvYWQoKXtcbiAgY29uc29sZS5sb2coXCJMb2FkIGNhbGxlZFwiKTtcbiAgICAgICAgdmFyIGxpbmsgPSAnaHR0cDovL3d3dy5hbnl0aW1lbGVhcm4uaW4vbWFQYWdlcy9nZXRFbmFibGVkUHJvZmlsZUlvbmljLnBocCc7XG4gICAgICAgIHZhciBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe3NpbUlEOmxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGV2aWNlSWRcIiksY29tcGFueVByb2ZpbGU6dGhpcy5jb21wYW55SWR9KTtcbiAgICAgICAgdGhpcy5zcWwuZGJjcmVhdGUoJ0FueXRpbWVMZWFybicsW1wiQ1JFQVRFIFRBQkxFIElGIE5PVCBFWElTVFMgYXNzZXNzbWVudChFRCBURVhULCBOYW1lIFRFWFQsIFF1ZXN0aW9ucyBURVhULFRJIFRFWFQgUFJJTUFSWSBLRVksVFEgVEVYVCxUUyBURVhUKVwiLFtdXSwoKT0+e30pO1xuICAgICAgICB0aGlzLnNxbC5kYmNyZWF0ZSgnQW55dGltZUxlYXJuJyxbXCJDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyBzdWJtaXRyZXN1bHRzKFRJIFRFWFQgUFJJTUFSWSBLRVksTElOSyBWQVJDSEFSKDIwMCksUkVTVUxUUyBWQVJDSEFSKDEwMDApLFJFU1BPTlNFIFZBUkNIQVIoMTAwMCkgREVGQVVMVCAtMSlcIixbXV0sKCk9Pnt9KTtcbiAgICAgICAgdGhpcy5odHRwLnBvc3QobGluaywgZGF0YSlcbiAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSlcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcbiAgICAgICAgICAgLy8gdGhpcy5kYXRhLnJlc3BvbnNlID0gZGF0YS5qc29uO1xuICAgICAgICAgICB0aGlzLnBvc3RzID0gZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XG5cbiAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgICAgY29uc29sZS5sb2coXCJPb29vcHMhXCIrZXJyb3IpO1xuICAgICAgICAgICAgdGhpcy5xdWVyeT1cIlNlbGVjdCAqIGZyb20gYXNzZXNzbWVudFwiOyAgICAgICAgIFxuICAgICAgICAgICAgdGhpcy50ZW1wcG9zdD10aGlzLnBvc3RzOyAgXG4gICAgICAgICAgICB0aGlzLnBvc3RzPVtdOyAgXG4gICAgICAgICAgICB0aGlzLnNxbC5kYmNyZWF0ZSgnQW55dGltZUxlYXJuJyxbdGhpcy5xdWVyeSxbXV0sKGRhdGEpPT57ICAgICAgICAgICAgXG4gICAgICAgICAgICAgIGZvcih2YXIgaT0wO2k8ZGF0YS5yb3dzLmxlbmd0aDtpKyspe1xuICAgICAgICAgICAgICAgIHRoaXMuc3RvcmVkdGVzdC5wdXNoKGRhdGEucm93cy5pdGVtKGkpLk5hbWUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGZvcih2YXIgaT0wO2k8dGhpcy50ZW1wcG9zdC5sZW5ndGg7aSsrKXtcbiAgICAgICAgICAgICAgICBpZih0aGlzLnN0b3JlZHRlc3QuaW5kZXhPZih0aGlzLnRlbXBwb3N0W2ldW1wiY19jblwiXSkhPS0xKVxuICAgICAgICAgICAgICAgICAgIHRoaXMucG9zdHMucHVzaCh0aGlzLnRlbXBwb3N0W2ldKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMucG9zdHMpO1xuICAgICAgICAgICAgfSk7ICAgXG4gICAgICAgICAgfSk7XG4gICAgfVxuXG4gbG9hZFRlc3QocG9zdFRlc3QpIHtcbiAgIGNvbnNvbGUubG9nKFwicG9zdFRlc3RcIixwb3N0VGVzdCk7XG4gICAgaWYocG9zdFRlc3QuY19uQT09XCIxXCIgJiYgdGhpcy50ZXN0VHlwZS5pbmRleE9mKHBvc3RUZXN0LmNfY3QpIT0tMSApXG4gICAgICB0aGlzLmRpYWxvZy5kaXNwbGF5RGlhbG9nKFwiRW50ZXIgVXNlciBJZFwiLFwiXCIsXCJTdWJtaXRcIixcIlwiLCh0ZXh0KT0+e1xuICAgICAgICB0aGlzLnVzZXJfaWQ9dGV4dDtcbiAgICAgICAgaWYodGV4dD09XCJcIiB8fCB0ZXh0LnRvU3RyaW5nKCkubWF0Y2goL15bYS16MC05XSskL2kpPT1udWxsKVxuICAgICAgICAgIHRoaXMuZGlhbG9nLmRpYWxvZ3MuYWxlcnQoXCJFbnRlciBhIHZhbGlkIFVzZXIgSURcIik7XG4gICAgICAgIGVsc2V7XG4gICAgICAgICAgdGhpcy5uYXZDdHJsLnB1c2goVGVzdFBhZ2UsIHtcbiAgICAgICAgICAgIFwiY291cnNlSWRcIjpwb3N0VGVzdC5jX2lkLFxuICAgICAgICAgICAgXCJjb3Vyc2VfTmFtZVwiOnBvc3RUZXN0LmNfY24sXG4gICAgICAgICAgICBcInVzZXJfaWRcIjp0ZXh0LFxuICAgICAgICAgICAgXCJjb3Vyc2VfdHlwZVwiOnBvc3RUZXN0LmNfY3RcbiAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgIGVsc2UgaWYodGhpcy50ZXN0VHlwZS5pbmRleE9mKHBvc3RUZXN0WydjX2N0J10pIT0tMSl7XG4gICAgdGhpcy5uYXZDdHJsLnB1c2goVGVzdFBhZ2UsIHtcbiAgICAgIFwiY291cnNlSWRcIjpwb3N0VGVzdC5jX2lkLFxuICAgICAgXCJjb3Vyc2VfTmFtZVwiOnBvc3RUZXN0LmNfY24sXG4gICAgICBcInVzZXJfaWRcIjpudWxsLFxuICAgICAgXCJjb3Vyc2VfdHlwZVwiOnBvc3RUZXN0LmNfY3QgIFxuICAgIH0pXG4gIH1cbiAgfVxuXG5cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0LnRzIiwiaW1wb3J0IHsgQ29tcG9uZW50LE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uaWNQYWdlLCBOYXZDb250cm9sbGVyLCBOYXZQYXJhbXMgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7IEh0dHAgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvbWFwJztcbmltcG9ydCB7VG9hc3RzZXJ2aWNlfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc2VydmljZXMvdG9hc3QnO1xuaW1wb3J0IHtTcWxpdGVEYlByb3ZpZGVyfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc3FsaXRlLWRiL3NxbGl0ZS1kYic7XG5pbXBvcnQgeyBIb21lUGFnZSB9IGZyb20gJy4uL2hvbWUvaG9tZSc7XG5pbXBvcnQgeyBVbmlxdWVEZXZpY2VJRCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvdW5pcXVlLWRldmljZS1pZCc7XG5pbXBvcnQge0xvYWRpbmdDb250cm9sbGVyfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7IFBsYXRmb3JtIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQge0RpYWxvZ1Byb3ZpZGVyfSBmcm9tICcuLi8uLi9wcm92aWRlcnMvc2VydmljZXMvZGlhbG9nJztcbi8qKlxuICogR2VuZXJhdGVkIGNsYXNzIGZvciB0aGUgVGVzdFBhZ2UgcGFnZS5cbiAqIFNlZSBodHRwOi8vaW9uaWNmcmFtZXdvcmsuY29tL2RvY3MvY29tcG9uZW50cy8jbmF2aWdhdGlvbiBmb3IgbW9yZSBpbmZvXG4gKiBvbiBJb25pYyBwYWdlcyBhbmQgbmF2aWdhdGlvbi5cbiAqL1xuXG5ASW9uaWNQYWdlKClcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BhZ2UtdGVzdCcsXG4gIHRlbXBsYXRlVXJsOiAndGVzdC5odG1sJyxcbn0pXG5cbmV4cG9ydCBjbGFzcyBUZXN0UGFnZSBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG4gIGluZGV4OiBudW1iZXIgPSAwO1xuICBnZXRPcHRuOiBhbnk7IFxuICBnZXRRdWVzbkxpc3Q6IGFueTtcbiAgZ2V0UXVlc246YW55O1xuICBjb3Vyc2VfaWQ6IGFueTtcbiAgY291cnNlX25hbWU6IGFueTtcbiAgY3VycmVudFBhZ2U6IGFueT0xO1xuICBhbnN3ZXI6YW55PS0xO1xuICBhbnN3ZXJzOmFueT1bXTtcbiAgcnVudGltZTpudW1iZXI9MDtcbiAgdGltZWxlZnQ6c3RyaW5nO1xuICB0YXNrOmFueTtcbiAgcXVlcnk6c3RyaW5nO1xuICByZXN1bHRkYXRhOmFueTtcbiAgbXVsdGlzZWxlY3Rfb3B0aW9uczphbnk9W107XG4gIHNob3dDaGVja2JveDpudW1iZXI7XG4gIGFuc3dlcl9tdWx0aTphbnk9W107XG4gIHVzZXJfaWQ6YW55O1xuICB0ZXN0X2lkOmFueTtcbiAgZGV2aWNlX2lkOmFueTtcbiAgbG9hZGVyOmFueTtcbiAgcmVzdWx0SlNPTjphbnlbXT1uZXcgQXJyYXkoKTtcbiAgY291cnNlVHlwZTpzdHJpbmc7XG4gIHRhYkJhckVsZW1lbnQ6YW55O1xuICB0ZXN0RHVyYXRpb246bnVtYmVyPTMwMDAwO1xuICBzdWJtaXR0ZWQ6Ym9vbGVhbj1mYWxzZTtcbiAgcHVibGljIHF1ZXNOdW06IG51bWJlciA9MDtcbiAgcHJpdmF0ZSB0b3RhbFBhZ2VzOm51bWJlciA9IDA7XG4gIHByaXZhdGUgdGVzdFR5cGU6IGFueSA7XG4gIHNob3dUaW1lVGVzdFR5cGU6c3RyaW5nW109WydFJywnUycsJ1YnXTtcbiAgdG90YWxUaW1lOnN0cmluZz1cIjAwOjAwOjAwXCI7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGRpYWxvZzpEaWFsb2dQcm92aWRlcixwdWJsaWMgcGxhdGZvcm06UGxhdGZvcm0scHVibGljIGxvYWRpbmdDb250cm9sbGVyOkxvYWRpbmdDb250cm9sbGVyLHB1YmxpYyBzcWw6U3FsaXRlRGJQcm92aWRlcixwdWJsaWMgdG9hc3Q6VG9hc3RzZXJ2aWNlLHB1YmxpYyBuYXZDdHJsOiBOYXZDb250cm9sbGVyLCBwdWJsaWMgbmF2UGFyYW1zOiBOYXZQYXJhbXMscHVibGljIGh0dHA6IEh0dHApIHtcbiAgICBpZihwbGF0Zm9ybS5pcygnaW9zJykpe1xuICAgIHRoaXMubG9hZGVyPXRoaXMubG9hZGluZ0NvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgIGNvbnRlbnQ6XCJQbGVhc2UgV2FpdC4uXCIsXG4gICAgICBzaG93QmFja2Ryb3A6dHJ1ZSxcbiAgICAgIHNwaW5uZXI6J2lvcydcbiAgICB9KTtcbiAgfVxuICBpZihwbGF0Zm9ybS5pcygnYW5kcm9pZCcpKXtcbiAgICB0aGlzLmxvYWRlcj10aGlzLmxvYWRpbmdDb250cm9sbGVyLmNyZWF0ZSh7XG4gICAgICBjb250ZW50OlwiUGxlYXNlIFdhaXQuLlwiLFxuICAgICAgc2hvd0JhY2tkcm9wOnRydWUsXG4gICAgICBzcGlubmVyOidkb3RzJ1xuICAgIH0pO1xuICB9XG4gIFxuICAgIGlmKHR5cGVvZihsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRldmljZUlkXCIpKSE9J3VuZGVmaW5lZCcpXG4gICAgICB0aGlzLmRldmljZV9pZD0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkZXZpY2VJZFwiKTtcbiAgICBlbHNlXG4gICAgICB0aGlzLmRldmljZV9pZD1udWxsO1xuICAgIH1cblxuICBuZ09uRGVzdHJveSgpe1xuICAgIGlmKHRoaXMuc3VibWl0dGVkPT1mYWxzZSl7XG4gICAgICAgIHRoaXMuc3VibWl0KGZhbHNlKTtcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLnRhc2spO1xuICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7ICBcbiAgICAgIH1lbHNle1xuICAgIGNvbnNvbGUubG9nKFwiT24gRGVzdG9yeSBjYWxsZWQgb24gdGVzdC50c1wiKTtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy50YXNrKTtcbiAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICB9XG4gICAgfVxuXG4gIGlvblZpZXdEaWRMb2FkKCkge1xuICAgIC8vIHdvcmtzIGxpa2UgaW50ZW50IGdldHMgdGhlIGRhdHMgZnJvbSBjb3Vyc2UgbGlzdGluZyBwYWdlIFxuICAgIHRoaXMuY291cnNlX2lkID0gdGhpcy5uYXZQYXJhbXMuZ2V0KCdjb3Vyc2VJZCcpO1xuICAgIHRoaXMuY291cnNlX25hbWUgPSB0aGlzLm5hdlBhcmFtcy5nZXQoJ2NvdXJzZV9OYW1lJyk7XG4gICAgdGhpcy51c2VyX2lkPXRoaXMubmF2UGFyYW1zLmdldChcInVzZXJfaWRcIik7XG4gICAgdGhpcy5jb3Vyc2VUeXBlPXRoaXMubmF2UGFyYW1zLmdldChcImNvdXJzZV90eXBlXCIpO1xuICAgIGNvbnNvbGUubG9nKCdpb25WaWV3RGlkTG9hZCBUZXN0UGFnZScpO1xuICAgIC8vdGhpcy5nZXRUZXN0QXR0cmlidXRlcyh0aGlzLmNvdXJzZV9pZCk7XG4gICAgdGhpcy5sb2FkZXIucHJlc2VudCgpLnRoZW4oKCk9PntcbiAgICAgIHRoaXMuZ2V0UXVlc3Rpb24odGhpcy5jb3Vyc2VfaWQsdGhpcy5xdWVzTnVtKTtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkbmV4dCcpLnN0eWxlLmRpc3BsYXk9Jyc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3VibWl0Jykuc3R5bGUuZGlzcGxheT0nbm9uZSc7ICAgIFxuICAgICAgfSk7ICBcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy50YXNrKTsgICAgXG4gICAgaWYodGhpcy5zaG93VGltZVRlc3RUeXBlLmluZGV4T2YodGhpcy5jb3Vyc2VUeXBlKSE9LTEpICBcbiAgICAgIHtcbiAgICBjbGVhckludGVydmFsKHRoaXMudGFzayk7ICAgIFxuICAgIHRoaXMudGFzayA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgIHRoaXMudGltZWxlZnQ9dGhpcy5yZWZyZXNoRGF0YSgpK1wiL1wiK3RoaXMudG90YWxUaW1lO1xuICAgIH0sIDEwMDApO31cbiAgICB0aGlzLnRhYkJhckVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudGFiYmFyLnNob3ctdGFiYmFyJyk7XG4gICAgdGhpcy50YWJCYXJFbGVtZW50LnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gIH1cblxuXG4gIHJlZnJlc2hEYXRhKCkge1xuICAgIHRoaXMucnVudGltZSs9MTAwMDtcbiAgICBpZih0aGlzLnJ1bnRpbWU+PXRoaXMudGVzdER1cmF0aW9uKXtcbiAgICAgIGNvbnNvbGUubG9nKFwiVGVzdCBEb25lXCIpO1xuICAgICAgdGhpcy5zdWJtaXQodHJ1ZSk7XG4gICAgICBjbGVhckludGVydmFsKHRoaXMudGFzayk7XG4gICAgICB0aGlzLm5hdkN0cmwucG9wKCk7XG4gICAgfVxuICAgIHZhciBtaWxsaXNlYz10aGlzLnJ1bnRpbWU7XG4gICAgdmFyIGhvdXIsbWludXRlcyxzZWNvbmRzO1xuICAgIGlmKG1pbGxpc2VjPj0zNjAwMDAwKXtcbiAgICAgIGhvdXI9JzAwJytNYXRoLmZsb29yKG1pbGxpc2VjLyg2MCo2MCoxMDAwKSk7XG4gICAgICBtaWxsaXNlYz1taWxsaXNlYy0oaG91cio2MCo2MCoxMDAwKTtcbiAgICB9XG4gICAgZWxzZVxuICAgICAgaG91cj0nMDAnKzA7XG4gICAgaWYobWlsbGlzZWM+PTYwMDAwKXtcbiAgICAgIG1pbnV0ZXM9JzAwJytNYXRoLmZsb29yKG1pbGxpc2VjLyg2MCoxMDAwKSk7XG4gICAgICBtaWxsaXNlYz1taWxsaXNlYy0obWludXRlcyo2MCoxMDAwKTtcbiAgICB9XG4gICAgZWxzZSBtaW51dGVzPScwMCcrMDtcbiAgICBpZihtaWxsaXNlYz49MTAwMCl7XG4gICAgICBzZWNvbmRzPScwMCcrTWF0aC5mbG9vcihtaWxsaXNlYy8xMDAwKTtcbiAgICB9XG4gICAgZWxzZSBcbiAgICAgIHNlY29uZHM9JzAwJyswO1xuICAgIGhvdXI9aG91ci5zdWJzdHIoaG91ci5sZW5ndGgtMixob3VyLmxlbmd0aC0xKTtcbiAgICBtaW51dGVzPW1pbnV0ZXMuc3Vic3RyKG1pbnV0ZXMubGVuZ3RoLTIsbWludXRlcy5sZW5ndGgtMSk7XG4gICAgc2Vjb25kcz1zZWNvbmRzLnN1YnN0cihzZWNvbmRzLmxlbmd0aC0yLHNlY29uZHMubGVuZ3RoLTEpOyAgIFxuICAgIHJldHVybiBob3VyKyc6JyttaW51dGVzKyc6JytzZWNvbmRzO1xuICB9XG4gIC8vZ2V0IHRoZSBxdWVzdGlvbiBmb3IgdGVtcCwgd2lsbCBtb3ZlIHRvIHByb3ZpZGVyIHBhZ2UgbGF0ZXJcbiAgZ2V0UXVlc3Rpb24oaWQ6YW55ICwgcGFnZU5vOiBhbnkpe1xuICAgIGNvbnNvbGUubG9nKHRoaXMuZGV2aWNlX2lkICApO1xuICAgIHZhciBsaW5rID0gJ2h0dHA6Ly93d3cuYW55dGltZWxlYXJuLmluL21hUGFnZXMvZ2V0VGVzdEF0dHJpYkFuZFF1ZXN0aW9uc0lvbmljLnBocCc7XG4gICAgICAgICAgdmFyIGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7Q291cnNlSWQ6aWQsU2ltSWQ6dGhpcy5kZXZpY2VfaWR9KTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgICAgICB0aGlzLmh0dHAucG9zdChsaW5rLCBkYXRhKVxuICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpXG4gICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcbiAgICAgICAgICAgICAvLyB0aGlzLmRhdGEucmVzcG9uc2UgPSBkYXRhO1xuICAgICAgICAgICAgICB0aGlzLnF1ZXJ5PScgIEluc2VydCBPUiBSZXBsYWNlIGludG8gYXNzZXNzbWVudChFRCxOYW1lLFF1ZXN0aW9ucyxUSSxUUSxUUykgVmFsdWVzKD8sPyw/LD8sPyw/KSc7XG4gICAgICAgICAgICAgIHRoaXMuc3FsLmRiY3JlYXRlKCdBbnl0aW1lTGVhcm4nLFt0aGlzLnF1ZXJ5LFtkYXRhLmxpc3QuRUQsZGF0YS5saXN0Lk5hbWUsSlNPTi5zdHJpbmdpZnkoZGF0YS5saXN0LlF1ZXN0aW9ucyksZGF0YS5saXN0LlRJLGRhdGEubGlzdC5UUSxkYXRhLmxpc3QuVFNdXSwoKT0+e30pO1xuICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlEYkRhdGEoaWQpO1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJPb29vcHMhXCIrZXJyb3IpO1xuICAgICAgICAgICAgICB0aGlzLnRvYXN0LnNob3dUb2FzdChcIk9mZmxpbmVcIik7XG4gICAgICAgICAgICAgIHRoaXMuZGlzcGxheURiRGF0YShpZCk7XG4gICAgICAgICAgICB9KTtcblxuXG4gIH1cblxuICBkaXNwbGF5RGJEYXRhKGlkKXtcbiAgICB0aGlzLnF1ZXJ5PVwiU2VsZWN0ICogZnJvbSBhc3Nlc3NtZW50IFdIRVJFIFRJPSdcIitpZCtcIidcIjsgICAgICAgICAgIFxuICAgIHRoaXMuc3FsLmRiY3JlYXRlKCdBbnl0aW1lTGVhcm4nLFt0aGlzLnF1ZXJ5LFtdXSwoZGF0YSk9PntcbiAgICAgdGhpcy5yZXN1bHRkYXRhPWRhdGEucm93cztcbiAgICAgZm9yKHZhciBpPTA7aTx0aGlzLnJlc3VsdGRhdGEuaXRlbS5sZW5ndGg7aSsrKXtcbiAgICAgY29uc29sZS5sb2codGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkpO1xuICAgICB0cnl7XG4gICAgIHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlF1ZXN0aW9ucz1KU09OLnBhcnNlKHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlF1ZXN0aW9ucyk7fVxuICAgICBjYXRjaChlcnIpe1xuICAgICAgIHRoaXMudG9hc3Quc2hvd1RvYXN0KFwiQ29ubmVjdGlvbiBub3QgYXZhaWxhYmxlXCIpO1xuICAgICAgIHRoaXMubmdPbkRlc3Ryb3koKTtcbiAgICAgICB0aGlzLm5hdkN0cmwucG9wVG9Sb290KCk7XG4gICAgICB9XG4gICAgICB2YXIgdGltZTphbnkgPSBcIjAwXCI7XG4gICAgICB0aW1lKz0gTWF0aC5mbG9vcihOdW1iZXIodGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuRUQpLzYwKTtcbiAgICAgIHRpbWU9dGltZS5zdWJzdHIodGltZS5sZW5ndGgtMix0aW1lLmxlbmd0aC0xKTtcbiAgICAgIHRoaXMudG90YWxUaW1lPXRpbWU7XG4gICAgICB0aGlzLnRlc3RfaWQ9dGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuVEk7XG4gICAgICB0aW1lPVwiMDBcIjtcbiAgICAgIHRpbWUrPU1hdGguZmxvb3IoTnVtYmVyKHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLkVEKS1OdW1iZXIodGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuRUQpLzYwKTtcbiAgICAgIHRpbWU9dGltZS5zdWJzdHIodGltZS5sZW5ndGgtMix0aW1lLmxlbmd0aC0xKTtcbiAgICAgIHRoaXMudG90YWxUaW1lKz0gXCI6XCIrdGltZStcIjowMFwiO1xuXG4gICAgICB0aGlzLnRlc3REdXJhdGlvbj1OdW1iZXIodGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuRUQpKjYwMDAwO1xuICAgICAgY29uc29sZS5sb2coXCJEdXJhdGlvbjpcIix0aGlzLnRlc3REdXJhdGlvbik7XG4gICAgICB0aGlzLmdldFF1ZXNuTGlzdCA9dGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuUXVlc3Rpb25zO1xuICAgICAgdGhpcy5zaG93Q2hlY2tib3g9dGhpcy5nZXRRdWVzbkxpc3RbdGhpcy5xdWVzTnVtXS5yQW5zLnNwbGl0KFwiLFwiKS5sZW5ndGg7XG4gICAgICBjb25zb2xlLmxvZyhcImxlbmd0aFwiLHRoaXMuc2hvd0NoZWNrYm94KTsgICAgXG4gICAgICB0aGlzLmdldFF1ZXNuID0gdGhpcy5nZXRRdWVzbkxpc3RbdGhpcy5xdWVzTnVtXS5xdWVuO1xuICAgICB0aGlzLmdldE9wdG4gPSB0aGlzLmdldFF1ZXNuTGlzdFt0aGlzLnF1ZXNOdW1dLm9wdG47XG4gICAgIHRoaXMudG90YWxQYWdlcz10aGlzLmdldFF1ZXNuTGlzdC5sZW5ndGg7ICBcbiAgICAgXG4gICAgIGlmKGk9PXRoaXMucmVzdWx0ZGF0YS5pdGVtLmxlbmd0aC0xKXtcbiAgICAgXG4gICAgZm9yKHZhciBpPTA7aTx0aGlzLnRvdGFsUGFnZXM7aSsrKVxuICAgIHRoaXMuYW5zd2Vycy5wdXNoKC0xKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTsgICAgICAgICAgICAgXG4gICBcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgfSk7XG4gIH1cblxuXG4gIGdldFRlc3RBdHRyaWJ1dGVzKGlkOmFueSl7XG4gICAgICAgIHZhciBsaW5rID0gJ2h0dHA6Ly93d3cuYW55dGltZWxlYXJuLmluL21hUGFnZXMvZ2V0VGVzdEF0dHJpYnV0ZXNJb25pYy5waHAnO1xuICAgICAgICB2YXIgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtDb3Vyc2VJZDppZCxTaW1JZDp0aGlzLmRldmljZV9pZH0pO1xuICAgICAgICB0aGlzLmh0dHAucG9zdChsaW5rLCBkYXRhKVxuICAgICAgICAubWFwKHJlcyA9PiByZXMuanNvbigpKVxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgICAvLyB0aGlzLmRhdGEucmVzcG9uc2UgPSBkYXRhO1xuICAgICAgIC8vICAgY29uc29sZS5sb2coZGF0YS5leHBsKTsgICAgICAgICAgIFxuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coXCJPb29vcHMhXCIrZXJyb3IpO1xuICAgICAgICB9KTtcbiAgfVxuXG4gIHN1Ym1pdChkb1BvcCl7XG4gICAgY29uc29sZS5sb2coZG9Qb3ApO1xuICAgIHRoaXMuc3VibWl0dGVkPXRydWU7XG4gICAgdGhpcy5hbnN3ZXJzW3RoaXMucXVlc051bV09dGhpcy5hbnN3ZXI7ICAgICAgXG4gICAgY29uc29sZS5sb2codGhpcy5hbnN3ZXJzKTtcbiAgICB2YXIgYW5zd2VyQXJyYXk9bmV3IEFycmF5KCk7XG4gICAgZm9yKHZhciBpPTA7aTx0aGlzLnRvdGFsUGFnZXM7aSsrKVxuICAgICAge1xuICAgICAgICBpZih0aGlzLmFuc3dlcnNbaV0hPS0xICYmICFBcnJheS5pc0FycmF5KHRoaXMuYW5zd2Vyc1tpXSkpXG4gICAgICAgICAgdGhpcy5hbnN3ZXJzW2ldPU51bWJlcih0aGlzLmFuc3dlcnNbaV0pKzE7XG4gICAgICAgIGlmKHRoaXMuYW5zd2Vyc1tpXSE9LTEgJiYgQXJyYXkuaXNBcnJheSh0aGlzLmFuc3dlcnNbaV0pKVxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMuYW5zd2Vyc1tpXT10aGlzLmFuc3dlcnNbaV0ubWFwKGZ1bmN0aW9uKHZhbCxrKXtcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodmFsKSsxO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBhbnN3ZXJBcnJheS5wdXNoKHtcbiAgICAgICAgICAgIFwiU2VxTm9cIjppLFxuICAgICAgICAgICAgIFwiUGFnZU5vXCI6dGhpcy5nZXRRdWVzbkxpc3RbaV1bXCJwYWdlTnVtXCJdLFxuICAgICAgICAgICAgXCJBbnN3ZXJcIjpBcnJheS5pc0FycmF5KHRoaXMuYW5zd2Vyc1tpXSk/dGhpcy5hbnN3ZXJzW2ldLmpvaW4oXCI6XCIpOnRoaXMuYW5zd2Vyc1tpXVxuICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgdGhpcy5yZXN1bHRKU09OLnB1c2goe1xuICAgICAgICBcIlNpbUlkXCI6dGhpcy5kZXZpY2VfaWQsXG4gICAgICAgIFwiQ291cnNlSWRcIjp0aGlzLmNvdXJzZV9pZCxcbiAgICAgICAgXCJVc2VySWRcIjp0aGlzLnVzZXJfaWQsXG4gICAgICAgIFwiQW5zd2VyTGlzdFwiOmFuc3dlckFycmF5XG4gICAgICB9KTtcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMucmVzdWx0SlNPTik7XG4gICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeSh0aGlzLmFuc3dlcnMpKTtcbiAgICAgIHZhciBsaW5rPVwiaHR0cDovL3d3dy5hbnl0aW1lbGVhcm4uaW4vbWFQYWdlcy90YWtlRXhhbUFuc3dlcnNJb25pYy5waHBcIjtcbiAgICAgIGlmKHRoaXMucGxhdGZvcm0uaXMoJ2lvcycpKXtcbiAgICAgICAgdGhpcy5sb2FkZXI9dGhpcy5sb2FkaW5nQ29udHJvbGxlci5jcmVhdGUoe1xuICAgICAgICAgIGNvbnRlbnQ6XCJQbGVhc2UgV2FpdC4uXCIsXG4gICAgICAgICAgc2hvd0JhY2tkcm9wOnRydWUsXG4gICAgICAgICAgc3Bpbm5lcjonaW9zJ1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIGlmKHRoaXMucGxhdGZvcm0uaXMoJ2FuZHJvaWQnKSl7XG4gXG4gICAgICAgICAgdGhpcy5sb2FkZXI9dGhpcy5sb2FkaW5nQ29udHJvbGxlci5jcmVhdGUoe1xuICAgICAgICAgICAgY29udGVudDpcIlBsZWFzZSBXYWl0Li5cIixcbiAgICAgICAgICAgIHNob3dCYWNrZHJvcDp0cnVlLFxuICAgICAgICAgIHNwaW5uZXI6J2RvdHMnXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG5cbiAgICAgIHRoaXMubG9hZGVyLnByZXNlbnQoKS50aGVuKCgpPT57XG4gICAgICB0aGlzLmh0dHAucG9zdChsaW5rLEpTT04uc3RyaW5naWZ5KHRoaXMucmVzdWx0SlNPTikpXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtpZihkYXRhW1wiX2JvZHlcIl09PVwiU3VjY2Vzc1wiKXtcbiAgICAgICAgdGhpcy5xdWVyeT0nICBJbnNlcnQgT1IgUmVwbGFjZSBpbnRvIHN1Ym1pdHJlc3VsdHMoVEksTElOSyxSRVNVTFRTLFJFU1BPTlNFKSBWYWx1ZXMoPyw/LD8sPyknO1xuICAgICAgICB0aGlzLnNxbC5kYmNyZWF0ZSgnQW55dGltZUxlYXJuJyxbdGhpcy5xdWVyeSxbdGhpcy50ZXN0X2lkLGxpbmssSlNPTi5zdHJpbmdpZnkodGhpcy5yZXN1bHRKU09OKSxkYXRhW1wiX2JvZHlcIl1dXSwoKT0+e1xuICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgICBpZihkb1BvcClcbiAgICAgICAgICB0aGlzLm5hdkN0cmwucG9wKCk7XG4gICAgICAgIH0pO31cbiAgICAgICAgZWxzZXtcbiAgICAgICAgICBpZihkYXRhW1wiX2JvZHlcIl09PVwiRVJST1JcIil7XG4gICAgICAgICAgICB0aGlzLnRvYXN0LnNob3dUb2FzdChcIlVzZXIgbm90IGVucm9sbGVkIGZvciB0ZXN0XCIpO1xuICAgICAgICAgICAgdGhpcy5xdWVyeT0nICBJbnNlcnQgT1IgUmVwbGFjZSBpbnRvIHN1Ym1pdHJlc3VsdHMoVEksTElOSyxSRVNVTFRTLFJFU1BPTlNFKSBWYWx1ZXMoPyw/LD8sPyknO1xuICAgICAgICAgIHRoaXMuc3FsLmRiY3JlYXRlKCdBbnl0aW1lTGVhcm4nLFt0aGlzLnF1ZXJ5LFt0aGlzLnRlc3RfaWQsbGluayxKU09OLnN0cmluZ2lmeSh0aGlzLnJlc3VsdEpTT04pLFwiLTFcIl1dLCgpPT57XG4gICAgICAgICAgICB0aGlzLmxvYWRlci5kaXNtaXNzKCk7XG4gICAgICAgICAgICB0aGlzLnRvYXN0LnNob3dUb2FzdChcIlNvbWV0aGluZyB3ZW50IHdyb25nLlJlc3RhcnQgdGhlIGFwcFwiKTsgICAgICAgICAgICBcbiAgICAgICBpZihkb1BvcClcbiAgICAgICAgICAgIHRoaXMubmF2Q3RybC5wb3AoKTtcbiAgICAgICAgICB9KTsgIFxuICAgICAgICB9XG4gICAgICAgIGlmKGRhdGFbXCJfYm9keVwiXT09XCJFUlJPUl9JTlZBTElEXCIpe1xuICAgICAgICAgIHRoaXMudG9hc3Quc2hvd1RvYXN0KFwiVXNlciBub3QgZW5yb2xsZWQgZm9yIHRlc3RcIik7XG4gICAgICAgICAgdGhpcy5sb2FkZXIuZGlzbWlzcygpO1xuICAgICAgIGlmKGRvUG9wKVxuICAgICAgICAgIHRoaXMubmF2Q3RybC5wb3AoKTtcbiAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgdGhpcy50b2FzdC5zaG93VG9hc3QoXCJTb21ldGhpbmcgd2VudCB3cm9uZy5Db250YWN0IEFkbWluXCIpO1xuICAgICAgICBjb25zb2xlLmxvZyhcIk9vb29wcyFcIitlcnJvcik7XG4gICAgICAgIHRoaXMucXVlcnk9JyAgSW5zZXJ0IE9SIFJlcGxhY2UgaW50byBzdWJtaXRyZXN1bHRzKFRJLExJTkssUkVTVUxUUyxSRVNQT05TRSkgVmFsdWVzKD8sPyw/LD8pJztcbiAgICAgICAgdGhpcy5zcWwuZGJjcmVhdGUoJ0FueXRpbWVMZWFybicsW3RoaXMucXVlcnksW3RoaXMudGVzdF9pZCxsaW5rLEpTT04uc3RyaW5naWZ5KHRoaXMucmVzdWx0SlNPTiksXCItMVwiXV0sKCk9PntcbiAgICAgICAgaWYoZG9Qb3ApXG4gICAgICAgICAgdGhpcy5uYXZDdHJsLnBvcCgpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIH1cblxuICAvL2xvYWQgbmV4dCBxdWVzdGlvblxuICBsb2FkTmV4dFF1ZXN0aW9uKCl7XG4gICAgY29uc29sZS5sb2codGhpcy5hbnN3ZXIpO1xuICAgIGlmKHRoaXMuYW5zd2VyPT11bmRlZmluZWQpXG4gICAgICB0aGlzLmFuc3dlcj0tMTtcbiAgICB0aGlzLmdldFF1ZXNuTGlzdFt0aGlzLnF1ZXNOdW1dLnJBbnMuc3BsaXQoXCIsXCIpLmxlbmd0aDsgICAgXG4gICAgaWYodGhpcy5xdWVzTnVtIT10aGlzLnRvdGFsUGFnZXMtMSl7XG4gICAgICBpZih0aGlzLmdldFF1ZXNuTGlzdFt0aGlzLnF1ZXNOdW1dLnJBbnMuc3BsaXQoXCIsXCIpLmxlbmd0aD4xKVxuICAgICAgICB0aGlzLmFuc3dlcj10aGlzLm11bHRpc2VsZWN0X29wdGlvbnM7XG4gICAgICB0aGlzLmFuc3dlcnNbdGhpcy5xdWVzTnVtXT10aGlzLmFuc3dlcjsgICAgICBcbiAgICAgIGNvbnNvbGUubG9nKFwiYmVmb3JlICsrXCIsdGhpcy5hbnN3ZXIpOyAgICAgICAgICAgXG4gICAgICB0aGlzLnF1ZXNOdW0rKztcbiAgICAgIHRoaXMuY3VycmVudFBhZ2UrKztcbiAgICAgICAgdGhpcy5tdWx0aXNlbGVjdF9vcHRpb25zPVtdOyAgICAgICBcbiAgICAgIHRoaXMuc2hvd0NoZWNrYm94PXRoaXMuZ2V0UXVlc25MaXN0W3RoaXMucXVlc051bV0uckFucy5zcGxpdChcIixcIikubGVuZ3RoOyAgICAgIFxuICAgICAgY29uc29sZS5sb2codGhpcy5hbnN3ZXIpO1xuICAgICAgY29uc29sZS5sb2codGhpcy5hbnN3ZXJzKTtcbiAgICAgIHRoaXMuZ2V0UXVlc24gPSB0aGlzLmdldFF1ZXNuTGlzdFt0aGlzLnF1ZXNOdW1dLnF1ZW47XG4gICAgdGhpcy5nZXRPcHRuID0gdGhpcy5nZXRRdWVzbkxpc3RbdGhpcy5xdWVzTnVtXS5vcHRuO1xuICAgIHRoaXMuYW5zd2VyPXRoaXMuYW5zd2Vyc1t0aGlzLnF1ZXNOdW1dOyAgICAgICAgXG4gICAgY29uc29sZS5sb2coXCJhZnRlciArK1wiLHRoaXMuYW5zd2VyKTsgICAgICAgICAgICBcbiAgICBpZihBcnJheS5pc0FycmF5KHRoaXMuYW5zd2VyKSlcbiAgICB0aGlzLm11bHRpc2VsZWN0X29wdGlvbnM9dGhpcy5hbnN3ZXI7ICAgICAgICAgIFxuICB9XG4gIGlmKHRoaXMucXVlc051bT09dGhpcy50b3RhbFBhZ2VzLTEpXG4gIHtcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3VibWl0Jykuc3R5bGUuZGlzcGxheT0nJztcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZG5leHQnKS5zdHlsZS5kaXNwbGF5PSdub25lJzsgICAgXG4gIH1cbiAgfVxuICBtdWx0aXNlbGVjdChldmVudCl7XG4gICAgaWYodGhpcy5tdWx0aXNlbGVjdF9vcHRpb25zLmluZGV4T2YoZXZlbnQpIT0tMSlcbiAgICAgIHRoaXMubXVsdGlzZWxlY3Rfb3B0aW9ucy5zcGxpY2UodGhpcy5tdWx0aXNlbGVjdF9vcHRpb25zLmluZGV4T2YoZXZlbnQpLDEpO1xuICAgIGVsc2VcbiAgICAgIHRoaXMubXVsdGlzZWxlY3Rfb3B0aW9ucy5wdXNoKGV2ZW50KTtcbiAgICBjb25zb2xlLmxvZyh0aGlzLm11bHRpc2VsZWN0X29wdGlvbnMpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMuYW5zd2VyKTtcbiAgICB9XG4gIC8vbG9hZCBwcmV2aW91cyBxdWVzdGlvbiAgXG5cbiAgaXNDaGVja2VkKHZhbCl7XG4gLy8gICBjb25zb2xlLmxvZyhcImNoZWNraW5nIGlmIGlzIGNoZWNrZWRcIik7XG4gICAgaWYodGhpcy5tdWx0aXNlbGVjdF9vcHRpb25zLmluZGV4T2YodmFsKSE9LTEpXG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICBlbHNlXG4gICAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBsb2FkUHJlUXVlc3Rpb24oKXsgXG4gICAgaWYodGhpcy5xdWVzTnVtPT0wKVxuICAgICAgdGhpcy50b2FzdC5zaG93VG9hc3QoXCJZb3UndmUgcmVhY2hlZCB0aGUgZmlyc3QgcXVlc3Rpb25cIik7XG4gIGlmKHRoaXMucXVlc051bT09dGhpcy50b3RhbFBhZ2VzLTEpXG4gICAge1xuICAgICAgdGhpcy5hbnN3ZXJzW3RoaXMucXVlc051bV09dGhpcy5hbnN3ZXI7ICAgICAgICAgXG4gICAgfVxuICAgIGlmKHRoaXMucXVlc051bT4wKXtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdsb2FkbmV4dCcpLnN0eWxlLmRpc3BsYXk9Jyc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3VibWl0Jykuc3R5bGUuZGlzcGxheT0nbm9uZSc7ICAgICBcbiAgICAgIGNvbnNvbGUubG9nKFwiYmVmb3JlIC0tXCIsdGhpcy5hbnN3ZXIpO1xuICAgICAgdGhpcy5xdWVzTnVtLS07XG4gICAgICB0aGlzLmN1cnJlbnRQYWdlLS07XG4gICAgICB0aGlzLnNob3dDaGVja2JveD10aGlzLmdldFF1ZXNuTGlzdFt0aGlzLnF1ZXNOdW1dLnJBbnMuc3BsaXQoXCIsXCIpLmxlbmd0aDsgICAgICBcbiAgICAgIHRoaXMuYW5zd2VyPXRoaXMuYW5zd2Vyc1t0aGlzLnF1ZXNOdW1dO1xuICAgICAgY29uc29sZS5sb2coXCJhZnRlciAtLVwiLHRoaXMuYW5zd2VyKTsgICAgICBcbiAgICAgIGlmKHRoaXMuZ2V0UXVlc25MaXN0W3RoaXMucXVlc051bV0uckFucy5zcGxpdChcIixcIikubGVuZ3RoPjEpXG4gICAgICAgIHRoaXMubXVsdGlzZWxlY3Rfb3B0aW9ucz10aGlzLmFuc3dlcjtcbiAgICAgICB0aGlzLmdldFF1ZXNuID0gdGhpcy5nZXRRdWVzbkxpc3RbdGhpcy5xdWVzTnVtXS5xdWVuO1xuICAgIHRoaXMuZ2V0T3B0biA9IHRoaXMuZ2V0UXVlc25MaXN0W3RoaXMucXVlc051bV0ub3B0bjtcbiAgfVxuICBcbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL3Rlc3QvdGVzdC50cyIsImltcG9ydCB7IFRvYXN0Q29udHJvbGxlciB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgQ29tcG9uZW50LEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFRvYXN0c2VydmljZXtcbmNvbnN0cnVjdG9yKHByaXZhdGUgdG9hc3RDdHJsOiBUb2FzdENvbnRyb2xsZXIpIHtcbiAgICBjb25zb2xlLmxvZygndG9hc3QgY2FsbGVkJyk7XG59XG5zaG93VG9hc3QobWVzc2FnZSl7XG4gICAgbGV0IHRvYXN0PXRoaXMudG9hc3RDdHJsLmNyZWF0ZSh7XG4gICAgICAgIG1lc3NhZ2U6bWVzc2FnZSxcbiAgICAgICAgZHVyYXRpb246MzAwMCxcbiAgICAgICAgcG9zaXRpb246J2JvdHRvbSdcbiAgICB9KTtcbiAgICB0b2FzdC5vbkRpZERpc21pc3MoKCk9PntcbiAgICAgICAgY29uc29sZS5sb2coJ1RvYXN0IGNsb3NlZCcpO1xuICAgIH0pO1xuICAgIHRvYXN0LmRpc21pc3NBbGwoKTtcbiAgICB0b2FzdC5wcmVzZW50KCk7XG59XG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3Byb3ZpZGVycy9zZXJ2aWNlcy90b2FzdC50cyIsImZ1bmN0aW9uIHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dChyZXEpIHtcblx0Ly8gSGVyZSBQcm9taXNlLnJlc29sdmUoKS50aGVuKCkgaXMgdXNlZCBpbnN0ZWFkIG9mIG5ldyBQcm9taXNlKCkgdG8gcHJldmVudFxuXHQvLyB1bmNhdGNoZWQgZXhjZXB0aW9uIHBvcHBpbmcgdXAgaW4gZGV2dG9vbHNcblx0cmV0dXJuIFByb21pc2UucmVzb2x2ZSgpLnRoZW4oZnVuY3Rpb24oKSB7XG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJy5cIik7XG5cdH0pO1xufVxud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LmtleXMgPSBmdW5jdGlvbigpIHsgcmV0dXJuIFtdOyB9O1xud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQ7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dDtcbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5pZCA9IDExNjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9AYW5ndWxhci9jb3JlL0Bhbmd1bGFyIGxhenlcbi8vIG1vZHVsZSBpZCA9IDExNlxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJ2YXIgbWFwID0ge1xuXHRcIi4uL3BhZ2VzL2xvZ2luL2xvZ2luLm1vZHVsZVwiOiBbXG5cdFx0Mjc4LFxuXHRcdDJcblx0XSxcblx0XCIuLi9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0Lm1vZHVsZVwiOiBbXG5cdFx0Mjc3LFxuXHRcdDFcblx0XSxcblx0XCIuLi9wYWdlcy90ZXN0L3Rlc3QubW9kdWxlXCI6IFtcblx0XHQyNzksXG5cdFx0MFxuXHRdXG59O1xuZnVuY3Rpb24gd2VicGFja0FzeW5jQ29udGV4dChyZXEpIHtcblx0dmFyIGlkcyA9IG1hcFtyZXFdO1xuXHRpZighaWRzKVxuXHRcdHJldHVybiBQcm9taXNlLnJlamVjdChuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInLlwiKSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fLmUoaWRzWzFdKS50aGVuKGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkc1swXSk7XG5cdH0pO1xufTtcbndlYnBhY2tBc3luY0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tBc3luY0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQXN5bmNDb250ZXh0LmlkID0gMTU4O1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQXN5bmNDb250ZXh0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vc3JjIGxhenlcbi8vIG1vZHVsZSBpZCA9IDE1OFxuLy8gbW9kdWxlIGNodW5rcyA9IDMiLCJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgQWJvdXRQYWdlIH0gZnJvbSAnLi4vYWJvdXQvYWJvdXQnO1xuaW1wb3J0IHsgQ29udGFjdFBhZ2UgfSBmcm9tICcuLi9jb250YWN0L2NvbnRhY3QnO1xuaW1wb3J0IHsgSG9tZVBhZ2UgfSBmcm9tICcuLi9ob21lL2hvbWUnO1xuXG5AQ29tcG9uZW50KHtcbiAgdGVtcGxhdGVVcmw6ICd0YWJzLmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFRhYnNQYWdlIHtcbiAgXG4gIHRhYjFSb290ID0gSG9tZVBhZ2U7XG4gIHRhYjJSb290ID0gQWJvdXRQYWdlO1xuICB0YWIzUm9vdCA9IENvbnRhY3RQYWdlO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuXG4gIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy90YWJzL3RhYnMudHMiLCJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdkNvbnRyb2xsZXIgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1hYm91dCcsXG4gIHRlbXBsYXRlVXJsOiAnYWJvdXQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQWJvdXRQYWdlIHtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgbmF2Q3RybDogTmF2Q29udHJvbGxlcikge1xuXG4gIH1cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL2Fib3V0L2Fib3V0LnRzIiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOYXZDb250cm9sbGVyIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBFbWFpbENvbXBvc2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9lbWFpbC1jb21wb3Nlcic7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwYWdlLWNvbnRhY3QnLFxuICB0ZW1wbGF0ZVVybDogJ2NvbnRhY3QuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQ29udGFjdFBhZ2Uge1xuZW1haWxBdmFpbGFibGU6Ym9vbGVhbj1mYWxzZTtcbiAgY29uc3RydWN0b3IocHVibGljIGVtYWlsOkVtYWlsQ29tcG9zZXIscHVibGljIG5hdkN0cmw6IE5hdkNvbnRyb2xsZXIpIHtcbiAgICB0aGlzLmVtYWlsLmlzQXZhaWxhYmxlKCkudGhlbigoYXZhaWxhYmxlOmJvb2xlYW4pPT57XG4gICAgICBpZihhdmFpbGFibGUpe1xuICAgICAgICB0aGlzLmVtYWlsQXZhaWxhYmxlPWF2YWlsYWJsZTtcbiAgICAgIH1cbiAgICB9KVxuICB9XG4gIHNlbmRFbWFpbCgpe1xuICAgIGxldCBlbWFpbERhdGE9e1xuICAgICAgdG86XCJpd2FudHRvbGVhcm5AYW55dGltZWxlYXJuLmluXCIsXG4gICAgICBzdWJqZWN0OlwiUXVlcnlcIixcbiAgICAgIGJvZHk6XCJcIixcbiAgICAgIGlzSHRtbDp0cnVlXG4gICAgfTtcbiAgdGhpcy5lbWFpbC5vcGVuKGVtYWlsRGF0YSk7XG4gIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9wYWdlcy9jb250YWN0L2NvbnRhY3QudHMiLCJpbXBvcnQgeyBDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXBwLE5hdkNvbnRyb2xsZXIgfSBmcm9tICdpb25pYy1hbmd1bGFyJztcbmltcG9ydCB7UHJvZmlsZUxpc3RQYWdlfSBmcm9tICcuLi8uLi9wYWdlcy9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0JztcbmltcG9ydCB7TG9naW5QYWdlfSBmcm9tICcuLi8uLi9wYWdlcy9sb2dpbi9sb2dpbic7XG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5pbXBvcnQge1NxbGl0ZURiUHJvdmlkZXJ9IGZyb20gJy4uLy4uL3Byb3ZpZGVycy9zcWxpdGUtZGIvc3FsaXRlLWRiJztcbmltcG9ydCB7U2VydmljZXNQcm92aWRlcn0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3NlcnZpY2VzL3NlcnZpY2VzJztcbmltcG9ydCB7TG9hZGluZ0NvbnRyb2xsZXJ9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuXG5pbXBvcnQgeyBQbGF0Zm9ybSB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGFnZS1ob21lJyxcbiAgdGVtcGxhdGVVcmw6ICdob21lLmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVQYWdle1xuICBpbnRlcnZhbDphbnk7XG4gIHRhYkJhckVsZW1lbnQ6YW55O1xuICBwcm9maWxlczphbnk7XG4gIGRpc3BsYXlQcm9maWxlczphbnlbXT1uZXcgQXJyYXkoKTtcbiAgbG9hZGVyOmFueTtcbiAgY29uc3RydWN0b3IocHVibGljIHBsYXRmb3JtOlBsYXRmb3JtLCBwdWJsaWMgaHR0cDpIdHRwLHB1YmxpYyBsb2FkaW5nQ29udHJvbGxlcjpMb2FkaW5nQ29udHJvbGxlciwgcHVibGljIG5hdkN0cmw6TmF2Q29udHJvbGxlcixwdWJsaWMgc2VydmljZXM6U2VydmljZXNQcm92aWRlcil7XG4gICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcbiAgICB0aGlzLmludGVydmFsPXNldEludGVydmFsKHRoaXMuc2VydmljZXMuaHR0cEJhY2tnb3J1bmRSZXN1bHRQb3N0KCksMTAwMDApO1xuICAvL3JlbWVtYmVyIHRvIHB1dCB0aGUgYmVsb3cgYWxvbmcgd2l0aCBwcmVzZW50IGVsc2Ugbm90IGRpc3BsYXllZCBhZnRyIHRhYiBjaGFuZ2VcblxuICB9XG4gIGlvblZpZXdXaWxsRW50ZXIoKXtcbiAgICBjb25zb2xlLmxvZyhcIklvbiB2aWV3IHdpbGwgZW50ZXJcIik7XG4gICAgaWYodHlwZW9mKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpKT09XCJ1bmRlZmluZWRcIiB8fCBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInVzZXJMb2dnZWRJblwiKT09XCJmYWxzZVwifHxsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInVzZXJMb2dnZWRJblwiKT09bnVsbCApe1xuICAgICAvLyB0aGlzLm5hdkN0cmwucHVzaChMb2dpblBhZ2Use30sKCk9Pntjb25zb2xlLmxvZyhcImRpc3BsYXllZFwiKX0pO1xuICAgICAvLyB0aGlzLmFwcC5nZXRSb290TmF2KCkucHVzaChMb2dpblBhZ2UpO1xuICAgICB0aGlzLm5hdkN0cmwucHVzaChMb2dpblBhZ2UpO1xuICAgICBjb25zb2xlLmxvZyhcIndpbGxFbnRlciBjYWxsZWRcIik7IFxuICAgIH1cbiAgfVxuXG4gIGxvYWQoKXtcbiAgICBpZih3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0ZXN0VGFrZW5cIikhPW51bGwpXG4gICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ0ZXN0VGFrZW5cIixcIlwiKTtcbiAgICB0aGlzLmRpc3BsYXlQcm9maWxlcz1uZXcgQXJyYXkoKTtcbiAgICB2YXIgbGluaz1cImh0dHA6Ly93d3cuYW55dGltZWxlYXJuLmluL21hUGFnZXMvcHJvZmlsZUxpc3RJb25pYy5waHBcIjtcbiAgICB0aGlzLmh0dHAucG9zdChsaW5rLEpTT04uc3RyaW5naWZ5KHtzaW1JZDpsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRldmljZUlkXCIpfSkpXG4gICAgLm1hcChyZXM9PnJlcy5qc29uKCkpXG4gICAgLnN1YnNjcmliZShkYXRhPT57XG4gICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgIHRoaXMucHJvZmlsZXM9ZGF0YTtcbiAgICAgIGlmKHR5cGVvZih0aGlzLnByb2ZpbGVzKSE9XCJ1bmRlZmluZWRcIilcbiAgICAgICAge1xuICAgICAgICAgIHRoaXMubG9hZGVyLmRpc21pc3MoKTtcbiAgICAgICAgZm9yKHZhciBpPTA7aTx0aGlzLnByb2ZpbGVzLnByb2ZpbGVMaXN0Lmxlbmd0aDtpKyspe1xuICAgICAgICAgIHRoaXMucHJvZmlsZXMucHJvZmlsZUxpc3RbaV1bXCJpbWFnZVwiXT10aGlzLnByb2ZpbGVzLnVybCt0aGlzLnByb2ZpbGVzLnByb2ZpbGVMaXN0W2ldLmNvbXBhbnlfaWQrXCIucG5nXCJcbiAgICAgICAgICB0aGlzLmRpc3BsYXlQcm9maWxlcy5wdXNoKHRoaXMucHJvZmlsZXMucHJvZmlsZUxpc3RbaV0pO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGlzcGxheVByb2ZpbGVzKTsgIFxuICAgICAgICB9XG4gICAgICB9LGVycm9yPT57XG4gICAgICBjb25zb2xlLmxvZyhcIk9vb3BzIEVycm9yIGluIGdldHRpbmcgcHJvZmlsZXNcIik7XG4gICAgfSk7XG4gIH1cblxuICBzdWJtaXQoY29tcGFueUlkKXtcbiAgICBjb25zb2xlLmxvZyhjb21wYW55SWQpO1xuICAgIGlmKHR5cGVvZihjb21wYW55SWQpIT1cInVuZGVmaW5lZFwiIHx8IGNvbXBhbnlJZCE9bnVsbClcbiAgICAgIHRoaXMubmF2Q3RybC5wdXNoKFByb2ZpbGVMaXN0UGFnZSx7XG4gICAgICAgIFwiY29tcGFueUlkXCI6Y29tcGFueUlkXG4gICAgICB9KTtcbiAgfVxuXG4gIGlvblZpZXdEaWRFbnRlcigpe1xuICAgIGlmKHRoaXMucGxhdGZvcm0uaXMoJ2lvcycpKXtcbiAgICAgIHRoaXMubG9hZGVyPXRoaXMubG9hZGluZ0NvbnRyb2xsZXIuY3JlYXRlKHtcbiAgICAgICAgY29udGVudDpcIlBsZWFzZSBXYWl0Li5cIixcbiAgICAgICAgc2hvd0JhY2tkcm9wOnRydWUsXG4gICAgICAgIHNwaW5uZXI6J2lvcydcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZih0aGlzLnBsYXRmb3JtLmlzKCdhbmRyb2lkJykpe1xuICAgICAgdGhpcy5sb2FkZXI9dGhpcy5sb2FkaW5nQ29udHJvbGxlci5jcmVhdGUoe1xuICAgICAgICBjb250ZW50OlwiUGxlYXNlIFdhaXQuLlwiLFxuICAgICAgICBzaG93QmFja2Ryb3A6dHJ1ZSxcbiAgICAgICAgc3Bpbm5lcjonZG90cydcbiAgICAgIH0pO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZyhcIklvbiB2aWV3IGRpZCBlbnRlclwiKTtcbiAgICB0aGlzLnRhYkJhckVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudGFiYmFyLnNob3ctdGFiYmFyJyk7XG4gICAgdGhpcy50YWJCYXJFbGVtZW50LnN0eWxlLmRpc3BsYXkgPSAnZmxleCc7XG4gICAgaWYodHlwZW9mKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpKSE9XCJ1bmRlZmluZWRcIiAmJiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInVzZXJMb2dnZWRJblwiKSE9XCJmYWxzZVwiICYmIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidXNlckxvZ2dlZEluXCIpIT1udWxsICl7XG4gICAgICB0aGlzLmxvYWRlci5wcmVzZW50KCkudGhlbigoKT0+e1xuICAgICAgICB0aGlzLmxvYWQoKTsgICAgICBcbiAgICAgIH0pLmNhdGNoKChlcnJsb2FkZXIpPT57XG4gICAgICAgIGNvbnNvbGUubG9nKGVycmxvYWRlci5jb2RlKTtcbiAgICAgIH0pOyAgICBcbiAgICB9XG4gIH1cblxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL2hvbWUvaG9tZS50cyIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHAgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvbWFwJztcbmltcG9ydCB7U3FsaXRlRGJQcm92aWRlcn0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3NxbGl0ZS1kYi9zcWxpdGUtZGInO1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNlcnZpY2VzUHJvdmlkZXIge1xucmVzdWx0ZGF0YTphbnk7XG5xdWVyeTphbnk7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBodHRwOiBIdHRwLHB1YmxpYyBzcWxpdGU6U3FsaXRlRGJQcm92aWRlcikge1xuICAgIGNvbnNvbGUubG9nKCdIZWxsbyBTZXJ2aWNlc1Byb3ZpZGVyIFByb3ZpZGVyJyk7XG4gIH1cbiAgaHR0cEJhY2tnb3J1bmRSZXN1bHRQb3N0KCl7XG4gICAgdGhpcy5zcWxpdGUuZGJjcmVhdGUoJ0FueXRpbWVMZWFybicsW1wiU2VsZWN0ICogZnJvbSBzdWJtaXRyZXN1bHRzIHdoZXJlIFJFU1BPTlNFPSctMSdcIixbXV0sKGRhdGEpPT57XG4gICAgICB0aGlzLnJlc3VsdGRhdGE9ZGF0YS5yb3dzO1xuICAgICAgZm9yKHZhciBpPTA7aTx0aGlzLnJlc3VsdGRhdGEuaXRlbS5sZW5ndGg7aSsrKXtcbiAgICAgICAgY29uc29sZS5sb2coXCJmcm9tIHNlcnZpY2VzXCIsdGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkpO1xuICAgICAgICB0aGlzLmh0dHAucG9zdCh0aGlzLnJlc3VsdGRhdGEuaXRlbShpKS5MSU5LLHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlJFU1VMVFMpLnN1YnNjcmliZShkYXRhPT57XG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICAgICAgaWYoZGF0YVtcIl9ib2R5XCJdPT1cIlN1Y2Nlc3NcIil7XG4gICAgICAgICAgdGhpcy5xdWVyeT0nICBJbnNlcnQgT1IgUmVwbGFjZSBpbnRvIHN1Ym1pdHJlc3VsdHMoVEksTElOSyxSRVNVTFRTLFJFU1BPTlNFKSBWYWx1ZXMoPyw/LD8sPyknO1xuICAgICAgICAgIHRoaXMuc3FsaXRlLmRiY3JlYXRlKCdBbnl0aW1lTGVhcm4nLFt0aGlzLnF1ZXJ5LFt0aGlzLnJlc3VsdGRhdGEuaXRlbShpKS5USSx0aGlzLnJlc3VsdGRhdGEuaXRlbShpKS5MSU5LLHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlJFU1VMVFMsZGF0YVtcIl9ib2R5XCJdXV0sKCk9PntcbiAgICAgICAgICB9KTt9XG4gICAgICAgICAgZWxzZXsgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMucXVlcnk9JyAgSW5zZXJ0IE9SIFJlcGxhY2UgaW50byBzdWJtaXRyZXN1bHRzKFRJLExJTkssUkVTVUxUUyxSRVNQT05TRSkgVmFsdWVzKD8sPyw/LD8pJztcbiAgICAgICAgICAgIHRoaXMuc3FsaXRlLmRiY3JlYXRlKCdBbnl0aW1lTGVhcm4nLFt0aGlzLnF1ZXJ5LFt0aGlzLnJlc3VsdGRhdGEuaXRlbShpKS5USSx0aGlzLnJlc3VsdGRhdGEuaXRlbShpKS5MSU5LLHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlJFU1VMVFMsXCItMVwiXV0sKCk9PntcbiAgICAgICAgICAgIH0pOyAgXG4gICAgICAgICAgfVxuICAgICAgICB9LGVycm9yPT57XG4gICAgICAgICAgY29uc29sZS5sb2coXCJPb29wcyBlcnJvclwiKTtcbiAgICAgICAgICB0aGlzLnF1ZXJ5PScgIEluc2VydCBPUiBSZXBsYWNlIGludG8gc3VibWl0cmVzdWx0cyhUSSxMSU5LLFJFU1VMVFMsUkVTUE9OU0UpIFZhbHVlcyg/LD8sPyw/KSc7XG4gICAgICAgICAgdGhpcy5zcWxpdGUuZGJjcmVhdGUoJ0FueXRpbWVMZWFybicsW3RoaXMucXVlcnksW3RoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLlRJLHRoaXMucmVzdWx0ZGF0YS5pdGVtKGkpLkxJTkssdGhpcy5yZXN1bHRkYXRhLml0ZW0oaSkuUkVTVUxUUyxcIi0xXCJdXSwoKT0+e1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3Byb3ZpZGVycy9zZXJ2aWNlcy9zZXJ2aWNlcy50cyIsImltcG9ydCB7IHBsYXRmb3JtQnJvd3NlckR5bmFtaWMgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyLWR5bmFtaWMnO1xuXG5pbXBvcnQgeyBBcHBNb2R1bGUgfSBmcm9tICcuL2FwcC5tb2R1bGUnO1xuXG5wbGF0Zm9ybUJyb3dzZXJEeW5hbWljKCkuYm9vdHN0cmFwTW9kdWxlKEFwcE1vZHVsZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvYXBwL21haW4udHMiLCJpbXBvcnQgeyBOZ01vZHVsZSwgRXJyb3JIYW5kbGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCcm93c2VyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XG5pbXBvcnQgeyBJb25pY0FwcCwgSW9uaWNNb2R1bGUsIElvbmljRXJyb3JIYW5kbGVyIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBNeUFwcCB9IGZyb20gJy4vYXBwLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBBYm91dFBhZ2UgfSBmcm9tICcuLi9wYWdlcy9hYm91dC9hYm91dCc7XG5pbXBvcnQgeyBDb250YWN0UGFnZSB9IGZyb20gJy4uL3BhZ2VzL2NvbnRhY3QvY29udGFjdCc7XG5pbXBvcnQgeyBIb21lUGFnZSB9IGZyb20gJy4uL3BhZ2VzL2hvbWUvaG9tZSc7XG5pbXBvcnQgeyBUYWJzUGFnZSB9IGZyb20gJy4uL3BhZ2VzL3RhYnMvdGFicyc7XG5pbXBvcnQgeyBUZXN0UGFnZSB9IGZyb20gJy4uL3BhZ2VzL3Rlc3QvdGVzdCc7XG5pbXBvcnQgeyBTdGF0dXNCYXIgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3N0YXR1cy1iYXInO1xuaW1wb3J0IHsgU3BsYXNoU2NyZWVuIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9zcGxhc2gtc2NyZWVuJztcbmltcG9ydCB7IFNlcnZpY2VzUHJvdmlkZXIgfSBmcm9tICcuLi9wcm92aWRlcnMvc2VydmljZXMvc2VydmljZXMnO1xuaW1wb3J0IHsgU3FsaXRlRGJQcm92aWRlciB9IGZyb20gJy4uL3Byb3ZpZGVycy9zcWxpdGUtZGIvc3FsaXRlLWRiJztcbmltcG9ydCB7IFNRTGl0ZX0gZnJvbSAnQGlvbmljLW5hdGl2ZS9zcWxpdGUnO1xuaW1wb3J0IHtUb2FzdHNlcnZpY2V9IGZyb20gJy4uL3Byb3ZpZGVycy9zZXJ2aWNlcy90b2FzdCc7XG5pbXBvcnQge0RpYWxvZ1Byb3ZpZGVyfSBmcm9tICcuLi9wcm92aWRlcnMvc2VydmljZXMvZGlhbG9nJztcbmltcG9ydCB7IERpYWxvZ3MgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2RpYWxvZ3MnO1xuaW1wb3J0IHsgVW5pcXVlRGV2aWNlSUQgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3VuaXF1ZS1kZXZpY2UtaWQnO1xuaW1wb3J0IHsgTG9naW5QYWdlIH0gZnJvbSAnLi4vcGFnZXMvbG9naW4vbG9naW4nO1xuaW1wb3J0IHsgUHJvZmlsZUxpc3RQYWdlIH0gZnJvbSAnLi4vcGFnZXMvcHJvZmlsZS1saXN0L3Byb2ZpbGUtbGlzdCc7XG5pbXBvcnQgeyBFbWFpbENvbXBvc2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9lbWFpbC1jb21wb3Nlcic7XG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBNeUFwcCxcbiAgICBBYm91dFBhZ2UsXG4gICAgQ29udGFjdFBhZ2UsXG4gICAgSG9tZVBhZ2UsXG4gICAgUHJvZmlsZUxpc3RQYWdlLFxuICAgIFRhYnNQYWdlLFxuICAgIFRlc3RQYWdlLFxuICAgIExvZ2luUGFnZVxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQnJvd3Nlck1vZHVsZSxcbiAgICBIdHRwTW9kdWxlLFxuICAgIElvbmljTW9kdWxlLmZvclJvb3QoTXlBcHApXG4gIF0sXG4gIGJvb3RzdHJhcDogW0lvbmljQXBwXSxcbiAgZW50cnlDb21wb25lbnRzOiBbXG4gICAgTXlBcHAsXG4gICAgQWJvdXRQYWdlLFxuICAgIENvbnRhY3RQYWdlLFxuICAgIExvZ2luUGFnZSxcbiAgICBIb21lUGFnZSxcbiAgICBQcm9maWxlTGlzdFBhZ2UsXG4gICAgVGFic1BhZ2UsXG4gICAgVGVzdFBhZ2VcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgVW5pcXVlRGV2aWNlSUQsXG4gICAgVG9hc3RzZXJ2aWNlLFxuICAgIFN0YXR1c0JhcixcbiAgICBTcGxhc2hTY3JlZW4sXG4gICAge3Byb3ZpZGU6IEVycm9ySGFuZGxlciwgdXNlQ2xhc3M6IElvbmljRXJyb3JIYW5kbGVyfSxcbiAgICBTZXJ2aWNlc1Byb3ZpZGVyLFxuICAgIFNxbGl0ZURiUHJvdmlkZXIsXG4gICAgRGlhbG9nUHJvdmlkZXIsXG4gICAgRW1haWxDb21wb3NlcixcbiAgICBTUUxpdGUsXG4gICAgRGlhbG9nc1xuICBdXG59KVxuZXhwb3J0IGNsYXNzIEFwcE1vZHVsZSB7fVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2FwcC9hcHAubW9kdWxlLnRzIiwiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBQbGF0Zm9ybSB9IGZyb20gJ2lvbmljLWFuZ3VsYXInO1xuaW1wb3J0IHsgU3RhdHVzQmFyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9zdGF0dXMtYmFyJztcbmltcG9ydCB7IFNwbGFzaFNjcmVlbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvc3BsYXNoLXNjcmVlbic7XG5pbXBvcnQgeyBTZXJ2aWNlc1Byb3ZpZGVyIH0gZnJvbSAnLi4vcHJvdmlkZXJzL3NlcnZpY2VzL3NlcnZpY2VzJztcbmltcG9ydCB7IFRhYnNQYWdlIH0gZnJvbSAnLi4vcGFnZXMvdGFicy90YWJzJztcblxuQENvbXBvbmVudCh7XG4gIHRlbXBsYXRlVXJsOiAnYXBwLmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIE15QXBwIHtcbiAgcm9vdFBhZ2U6YW55ID0gVGFic1BhZ2U7XG5cbiAgY29uc3RydWN0b3IocGxhdGZvcm06IFBsYXRmb3JtLCBzdGF0dXNCYXI6IFN0YXR1c0Jhciwgc3BsYXNoU2NyZWVuOiBTcGxhc2hTY3JlZW4pIHtcbiAgICBwbGF0Zm9ybS5yZWFkeSgpLnRoZW4oKCkgPT4ge1xuICAgICAgLy8gT2theSwgc28gdGhlIHBsYXRmb3JtIGlzIHJlYWR5IGFuZCBvdXIgcGx1Z2lucyBhcmUgYXZhaWxhYmxlLlxuICAgICAgLy8gSGVyZSB5b3UgY2FuIGRvIGFueSBoaWdoZXIgbGV2ZWwgbmF0aXZlIHRoaW5ncyB5b3UgbWlnaHQgbmVlZC5cbiAgICAgIHN0YXR1c0Jhci5zdHlsZURlZmF1bHQoKTtcbiAgICAgIHNwbGFzaFNjcmVlbi5oaWRlKCk7XG4gICAgfSk7XG4gIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9hcHAvYXBwLmNvbXBvbmVudC50cyIsImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgU1FMaXRlLCBTUUxpdGVPYmplY3QgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3NxbGl0ZSc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5cbi8qXG4gIEdlbmVyYXRlZCBjbGFzcyBmb3IgdGhlIFNxbGl0ZURiUHJvdmlkZXIgcHJvdmlkZXIuXG5cbiAgU2VlIGh0dHBzOi8vYW5ndWxhci5pby9kb2NzL3RzL2xhdGVzdC9ndWlkZS9kZXBlbmRlbmN5LWluamVjdGlvbi5odG1sXG4gIGZvciBtb3JlIGluZm8gb24gcHJvdmlkZXJzIGFuZCBBbmd1bGFyIERJLlxuKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTcWxpdGVEYlByb3ZpZGVyIHtcblxuICBzcWxpdGVvYmplY3Q6YW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNxbGl0ZTpTUUxpdGUpe1xuXG4gIH1cbiAgZGJjcmVhdGUoZGJuYW1lLHF1ZXJ5LGNhbGxiYWNrKXtcbiAgICAgIHRoaXMuc3FsaXRlLmNyZWF0ZSh7XG4gICAgICAgICAgbmFtZTpkYm5hbWUrJy5kYicsXG4gICAgICAgICAgbG9jYXRpb246J2RlZmF1bHQnXG4gICAgICB9KS50aGVuKChkYjpTUUxpdGVPYmplY3QpPT57XG4gICAgICBcbiAgICAgICAgZGIuZXhlY3V0ZVNxbChxdWVyeVswXSxxdWVyeVsxXSkudGhlbigoZGF0YSk9Pntjb25zb2xlLmxvZyhcIlNRTCBleGVjIHN1Y2Nlc3NmdWxsXCIsZGF0YSk7Y2FsbGJhY2soZGF0YSk7fSlcbiAgICAgICAgLmNhdGNoKGU9PmNvbnNvbGUubG9nKGUpKTtcbiAgICAgIH0pLmNhdGNoKGU9PmNvbnNvbGUubG9nKGUpKTtcbiAgXG59XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcHJvdmlkZXJzL3NxbGl0ZS1kYi9zcWxpdGUtZGIudHMiLCJpbXBvcnQgeyBEaWFsb2dzICxEaWFsb2dzUHJvbXB0Q2FsbGJhY2t9IGZyb20gJ0Bpb25pYy1uYXRpdmUvZGlhbG9ncyc7XG5pbXBvcnR7Q29tcG9uZW50LEluamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRGlhbG9nUHJvdmlkZXJcbntcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZGlhbG9nczpEaWFsb2dzKXtcbiAgICAgICAgXG4gICAgfVxuICAgIGRpc3BsYXlEaWFsb2cobWVzc2FnZSx0aXRsZSxidXR0b25MYWJlbCxoaW50LGNhbGxiYWNrKXtcbiAgICAgICAgdGhpcy5kaWFsb2dzLnByb21wdChtZXNzYWdlLHRpdGxlLGJ1dHRvbkxhYmVsLGhpbnQpXG4gICAgICAgIC50aGVuKCh0ZXh0KT0+e2NvbnNvbGUubG9nKFwiRGlhbG9nIGV4ZWN1dGVkXCIsdGV4dCk7Y2FsbGJhY2sodGV4dC5pbnB1dDEpO30pXG4gICAgICAgIC5jYXRjaCgoZXJyKT0+e2NvbnNvbGUubG9nKFwiRGlhbG9nIGVycm9yXCIpfSk7XG4gICAgfVxuICAgIGRpc3BsYXlDb25maXJtKG1lc3NhZ2U6c3RyaW5nLHRpdGxlOnN0cmluZyxidXR0b25MYWJlbDpzdHJpbmdbXSxjYWxsYmFjazphbnkpe1xuICAgICAgICB0aGlzLmRpYWxvZ3MuY29uZmlybShtZXNzYWdlLHRpdGxlLGJ1dHRvbkxhYmVsKVxuICAgICAgICAudGhlbigoY2xpY2tlZE9wdGlvbik9PntcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdDbGlja2VkIGlzJyxjbGlja2VkT3B0aW9uKTtcbiAgICAgICAgICAgIGNhbGxiYWNrKGNsaWNrZWRPcHRpb24pO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycik9Pntjb25zb2xlLmxvZyhcIkVycm9yIG9jY3VyZWQgaW4gY29uZmlybVwiKX0pO1xuICAgIH1cbn1cblxuXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcHJvdmlkZXJzL3NlcnZpY2VzL2RpYWxvZy50cyIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXBwLElvbmljUGFnZSwgTmF2Q29udHJvbGxlciwgTmF2UGFyYW1zIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBIdHRwIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBVbmlxdWVEZXZpY2VJRCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvdW5pcXVlLWRldmljZS1pZCc7XG5pbXBvcnQgeyBQcm9maWxlTGlzdFBhZ2UgfSBmcm9tICcuLi9wcm9maWxlLWxpc3QvcHJvZmlsZS1saXN0JztcbmltcG9ydCB7RGlhbG9nUHJvdmlkZXJ9IGZyb20gJy4uLy4uL3Byb3ZpZGVycy9zZXJ2aWNlcy9kaWFsb2cnO1xuXG4vKipcbiAqIEdlbmVyYXRlZCBjbGFzcyBmb3IgdGhlIExvZ2luUGFnZSBwYWdlLlxuICpcbiAqIFNlZSBodHRwczovL2lvbmljZnJhbWV3b3JrLmNvbS9kb2NzL2NvbXBvbmVudHMvI25hdmlnYXRpb24gZm9yIG1vcmUgaW5mbyBvblxuICogSW9uaWMgcGFnZXMgYW5kIG5hdmlnYXRpb24uXG4qL1xuXG5ASW9uaWNQYWdlKClcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BhZ2UtbG9naW4nLFxuICB0ZW1wbGF0ZVVybDogJ2xvZ2luLmh0bWwnLFxufSlcbmV4cG9ydCBjbGFzcyBMb2dpblBhZ2Uge1xuICB1c2VybmFtZTpzdHJpbmc7XG4gIHBob25lTnVtYmVyOnN0cmluZztcbiAgZW1haWxfZG9jOmFueTtcbiAgcGhvbmVOdW1iZXJfd3Jvbmc6Ym9vbGVhbj10cnVlO1xuICBlbWFpbF93cm9uZzpib29sZWFuPXRydWU7XG4gIGRpc2FibGVfc2lnbmluOmJvb2xlYW49ZmFsc2U7XG4gIHRhYkJhckVsZW1lbnQ6YW55O1xuICBjb21wYW55X2lkOmFueTtcbiAgdHlwZXNPZlVzZXJzOnN0cmluZ1tdPVtcIkknbSBhIFN0dWRlbnRcIixcIkknbSBhIGVtcGxveWVlXCIsXCJJJ20gYSBnZW5lcmFsIHVzZXJcIl07XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBkaWFsb2c6RGlhbG9nUHJvdmlkZXIscHVibGljIGFwcDpBcHAscHVibGljIGRldmljZUlkOlVuaXF1ZURldmljZUlELHB1YmxpYyBodHRwOkh0dHAsIHB1YmxpYyBuYXZDdHJsOiBOYXZDb250cm9sbGVyLCBwdWJsaWMgbmF2UGFyYW1zOiBOYXZQYXJhbXMpIHtcbiAgICBjb25zb2xlLmxvZyhcImNvbnN0cnVjdG9yXCIpO1xuICAgIFxuICB9XG5cbiAgc3VibWl0Q29tcGFueURhdGEodmFsKXtcbiAgICBjb25zb2xlLmxvZyh2YWwpO1xuICAgIHZhciBsaW5rPSdodHRwOi8vd3d3LmFueXRpbWVsZWFybi5pbi9tYVBhZ2VzL2RldmljZVJlZ2lzdHJhdGlvbklvbmljLnBocCc7XG4gICAgdmFyIGRhdGFHZW4gPSB7XG4gICAgICBzaW1JZDpsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRldmljZUlkXCIpLFxuICAgICAgZW1haWxJZDpcIlwiLFxuICAgICAgb3duUGhvbmU6XCJcIixcbiAgICAgIGNvbXBhbnlVbmlxdWVJZDowLFxuICAgICAgY29tcGFueV9pZDp0aGlzLmNvbXBhbnlfaWR9O1xuICAgIHN3aXRjaChOdW1iZXIodmFsKSl7XG4gICAgICBjYXNlIDE6aWYodGhpcy5lbWFpbF93cm9uZyE9dHJ1ZSAmJiB0aGlzLnBob25lTnVtYmVyX3dyb25nIT10cnVlKXtcbiAgICAgICAgZGF0YUdlbi5lbWFpbElkPXRoaXMudXNlcm5hbWU7XG4gICAgICAgIGRhdGFHZW4ub3duUGhvbmU9dGhpcy5waG9uZU51bWJlcjtcbiAgICAgIH1cbiAgICAgIGVsc2UgXG4gICAgICAgIHJldHVybjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIDI6aWYodGhpcy5waG9uZU51bWJlcl93cm9uZyE9dHJ1ZSl7XG4gICAgICAgIGRhdGFHZW4uZW1haWxJZD10aGlzLnBob25lTnVtYmVyK1wiQFwiK3RoaXMuY29tcGFueV9pZDsgICAgICAgIFxuICAgICAgICBkYXRhR2VuLm93blBob25lPXRoaXMucGhvbmVOdW1iZXI7XG4gICAgICB9XG4gICAgICBlbHNlIFxuICAgICAgICByZXR1cm47XG4gICAgICBicmVhaztcbiAgICAgIGNhc2UgMzppZih0aGlzLnBob25lTnVtYmVyX3dyb25nIT10cnVlKXtcbiAgICAgICAgZGF0YUdlbi5lbWFpbElkPXRoaXMudXNlcm5hbWUrXCJAXCIrdGhpcy5jb21wYW55X2lkO1xuICAgICAgICBkYXRhR2VuLm93blBob25lPXRoaXMucGhvbmVOdW1iZXI7XG4gICAgICB9XG4gICAgICBlbHNlIFxuICAgICAgICByZXR1cm47XG4gICAgICBicmVhaztcbiAgICAgIGNhc2UgNDppZih0aGlzLnBob25lTnVtYmVyX3dyb25nIT10cnVlKXtcbiAgICAgICAgZGF0YUdlbi5lbWFpbElkPXRoaXMudXNlcm5hbWUrXCJAXCIrdGhpcy5jb21wYW55X2lkO1xuICAgICAgICBkYXRhR2VuLm93blBob25lPXRoaXMucGhvbmVOdW1iZXI7XG4gICAgICB9XG4gICAgICBlbHNlIFxuICAgICAgICByZXR1cm47XG4gICAgICBicmVhaztcbiAgICB9XG4gICAgdGhpcy5kZXZpY2VJZC5nZXQoKVxuICAgIC50aGVuKChJZDphbnkpPT57XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRldmljZUlkXCIsSWQpO1xuICAgICAgZGF0YUdlbi5zaW1JZD1JZDtcbiAgICAgIHZhciBkYXRhID0gSlNPTi5zdHJpbmdpZnkoZGF0YUdlbik7XG4gICAgICBjb25zb2xlLmxvZyhkYXRhR2VuKTtcbiAgICAgIHRoaXMuaHR0cC5wb3N0KGxpbmssIGRhdGEpXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICAgICAgaWYoZGF0YVtcIl9ib2R5XCJdPT1cIlN1Y2Nlc3NcIil7XG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIixcInRydWVcIik7XG4gICAgICAgICAgdGhpcy5uYXZDdHJsLnBvcCgpOyAgICAgICAgIFxuICAgICAgICB9XG4gICAgICAgIHRoaXMudXNlcm5hbWU9Jyc7XG4gICAgICAgIHRoaXMucGhvbmVOdW1iZXI9Jyc7XG4gICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiT29vb3BzIVwiK2Vycm9yKTtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIixcImZhbHNlXCIpO1xuICAgICAgdGhpcy51c2VybmFtZT0nJztcbiAgICAgIHRoaXMucGhvbmVOdW1iZXI9Jyc7XG4gICAgICB9KTtcbiAgfSlcbiAgICAuY2F0Y2goKGVycik9PmNvbnNvbGUubG9nKFwiRXJyb3IgZ2V0dGluZyBkZXZpY2VJZFwiKSk7XG4gIH1cblxuICByYWRpb0NoYW5nZUhhbmRsZXIoZXZlbnQpe1xuICAgIGNvbnNvbGUubG9nKGV2ZW50KTtcbiAgICBzd2l0Y2goTnVtYmVyKGV2ZW50KSl7XG4gICAgICBjYXNlIDA6ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Rpc3BsYXlMaXN0Jykuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnU2lnbkluJykuc3R5bGUuZGlzcGxheT0nJztcbiAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAxOlxuICAgICAgdGhpcy5kaWFsb2cuZGlzcGxheUNvbmZpcm0oJ0RvIHlvdSBoYXZlIGFuIENvbXBhbnkgSUQnLCdNZXNzYWdlJyxbXCJZZXNcIixcIk5vXCJdLChjbGlja2VkKT0+e1xuICAgICAgICBzd2l0Y2goTnVtYmVyKGNsaWNrZWQpKXtcbiAgICAgICAgICBjYXNlIDA6Ly93aGVuIHVzZXIgY2xpY2tzIG91dHNpZGUgdGhlIGRpYWxvZy5EbyBub3RoaW5nIGF1dG8gaGFuZGxlXG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSAxOi8vd2hlbiB1c2VyIGNsaWNrcyB5ZXMgXG4gICAgICAgICAgdGhpcy5zZXREaXNwbGF5Rm9ybSgnRW1wbG95ZWVTaWduSW4nKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIDI6Ly93aGVuIHVzZXIgY2xpY2tzIG5vLkRvIG5vdGhpbmcgYXV0byBoYW5kbGVcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgY29uc29sZS5sb2coXCJDb25maXJtIGNvbXBsZXRlZFwiKTtcbiAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIDI6ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Rpc3BsYXlMaXN0Jykuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnU2lnbkluVXNlcicpLnN0eWxlLmRpc3BsYXk9Jyc7XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cbiAgaW9uVmlld0RpZExvYWQoKSB7XG4gICAgY29uc29sZS5sb2coJ2lvblZpZXdEaWRMb2FkIExvZ2luUGFnZScpO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdTaWduSW4nKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnU2lnbkluVXNlcicpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiRW1wbG95ZWVTaWduSW5cIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMVwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4yXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjNcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluNFwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICB0aGlzLnRhYkJhckVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcudGFiYmFyLnNob3ctdGFiYmFyJyk7XG4gICAgdGhpcy50YWJCYXJFbGVtZW50LnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gIH1cblxuICBzdWJtaXQodmFsKXtcbiAgICAgIFxuICAgICAgc3dpdGNoKE51bWJlcih2YWwpKXtcbiAgICAgICAgY2FzZSAwOiAgICAgICAgXG4gICAgICAgIGNhc2UgMTogICAgaWYodGhpcy5lbWFpbF93cm9uZyE9dHJ1ZSAmJiB0aGlzLnBob25lTnVtYmVyX3dyb25nIT10cnVlKVxuICAgICAgICB7XG4gICAgICAgIHZhciBsaW5rID0gJ2h0dHA6Ly93d3cuYW55dGltZWxlYXJuLmluL21hUGFnZXMvZGV2aWNlUmVnaXN0cmF0aW9uSW9uaWMucGhwJztcbiAgICB0aGlzLmRldmljZUlkLmdldCgpXG4gICAgICAudGhlbigoSWQ6YW55KT0+e1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRldmljZUlkXCIsSWQpO1xuICAgICAgICB2YXIgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICBzaW1JZDpsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRldmljZUlkXCIpLFxuICAgICAgICAgIGVtYWlsSWQ6dGhpcy51c2VybmFtZSxcbiAgICAgICAgICBvd25QaG9uZTp0aGlzLnBob25lTnVtYmVyLFxuICAgICAgICAgIGNvbXBhbnlVbmlxdWVJZDowLFxuICAgICAgICAgIGNvbXBhbnlfaWQ6MH0pO1xuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuICAgICAgICB0aGlzLmh0dHAucG9zdChsaW5rLCBkYXRhKVxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgICAgIGlmKGRhdGFbXCJfYm9keVwiXT09XCJTdWNjZXNzXCIpe1xuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIixcInRydWVcIik7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkRhdGEgIVwiK2RhdGEpO1xuICAgICAgICAgICAgdGhpcy5uYXZDdHJsLnBvcCgpOyAgICAgICAgIFxuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnVzZXJuYW1lPScnO1xuICAgICAgICAgIHRoaXMucGhvbmVOdW1iZXI9Jyc7XG4gICAgICAgIH0sIGVycm9yID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIk9vb29wcyFcIitlcnJvcik7XG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VyTG9nZ2VkSW5cIixcImZhbHNlXCIpO1xuICAgICAgICB0aGlzLnVzZXJuYW1lPScnO1xuICAgICAgICB0aGlzLnBob25lTnVtYmVyPScnO1xuICAgICAgICB9KTtcbiAgICB9KVxuICAgICAgLmNhdGNoKChlcnIpPT5jb25zb2xlLmxvZyhcIkVycm9yIGdldHRpbmcgZGV2aWNlSWRcIikpO1xuICAgIH0gICAgXG4gICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDI6XG4gICAgICAgIGNvbnNvbGUubG9nKFwidXNlcm5hbWVcIix0aGlzLnVzZXJuYW1lKTtcbiAgICAgICAgaWYodGhpcy51c2VybmFtZSE9XCIgXCIgfHx0eXBlb2YodGhpcy51c2VybmFtZSkhPW51bGwpe1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZXhlYyBzdGFydGVkXCIpO1xuICAgICAgICAgIHZhciBsaW5rID0gJ2h0dHA6Ly93d3cuYW55dGltZWxlYXJuLmluL21hUGFnZXMvZ2V0UmVnaXN0cmF0aW9uRmlsZWRzSW9uaWMucGhwJztcbiAgICAgICAgdGhpcy5jb21wYW55X2lkPXRoaXMudXNlcm5hbWU7XG4gICAgICAgICAgdmFyIGRhdGE9SlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgIGNvbXBhbnlJZDp0aGlzLnVzZXJuYW1lXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmh0dHAucG9zdChsaW5rLCBkYXRhKVxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiZXhlY1wiLGRhdGEpO1xuICAgICAgICAgICAgdGhpcy5zZXREaXNwbGF5Rm9ybShcIkNvbXBhbnlTaWduSW5cIitkYXRhW1wiX2JvZHlcIl0pO1xuICAgICAgICAgIHRoaXMudXNlcm5hbWU9Jyc7XG4gICAgICAgICAgdGhpcy5waG9uZU51bWJlcj0nJztcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiT29vb3BzIVwiK2Vycm9yKTtcbiAgICAgICAgICB0aGlzLnVzZXJuYW1lPScnO1xuICAgICAgICAgIHRoaXMucGhvbmVOdW1iZXI9Jyc7XG4gICAgICAgIH0pO31cbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgXG5cbiAgfVxuICB1c2VybmFtZWNoZWNrKHZhbCl7IFxuICAgIHN3aXRjaChOdW1iZXIodmFsKSl7XG4gICAgICBjYXNlIDA6IGlmKCF0aGlzLnVzZXJuYW1lLm1hdGNoKC9bYS16MC05XStbQF17MX1bYS16MC05XStbLl17MX1bYS16MC05XSsvaSkpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdlbWFpbGlkJykuc3R5bGUuYm9yZGVyQ29sb3I9XCJyZWRcIjtcbiAgICAgICAgdGhpcy5lbWFpbF93cm9uZz10cnVlO1xuICAgICAgICAgIH0gXG4gICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2VtYWlsaWQnKS5zdHlsZS5ib3JkZXJDb2xvcj1cIndoaXRlXCI7XG4gICAgICAgICAgdGhpcy5lbWFpbF93cm9uZz1mYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgICBicmVhazsgICBcbiAgICAgIGNhc2UgMTppZighdGhpcy5waG9uZU51bWJlci5tYXRjaCgvXlswLTldezEwfSQvKSlcbiAgICAgIHtcbiAgICAgICAgdGhpcy5waG9uZU51bWJlcl93cm9uZz10cnVlO1xuICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwYXNzX2RvYycpLnN0eWxlLmJvcmRlckNvbG9yPVwicmVkXCI7XG4gICAgICB9IFxuICAgICAgICAgIGVsc2V7XG4gICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Bhc3NfZG9jJykuc3R5bGUuYm9yZGVyQ29sb3I9XCJ3aGl0ZVwiO1xuICAgICAgICAgICAgdGhpcy5waG9uZU51bWJlcl93cm9uZz1mYWxzZTtcbiAgICAgICAgfVxuICAgICAgICAgIGJyZWFrOyAgXG4gICAgfVxuICB9XG4gIHNldERpc3BsYXlGb3JtKHZhbCl7XG4gICBpZih2YWw9PVwiZGlzcGxheUxpc3RcIil7XG4gICAgICBjb25zb2xlLmxvZyhcIlNpZ25JTlwiKTtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiU2lnbkluXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXNwbGF5TGlzdFwiKS5zdHlsZS5kaXNwbGF5PScnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJFbXBsb3llZVNpZ25JblwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjFcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4yXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluM1wiKS5zdHlsZS5kaXNwbGF5PSdub25lJzsgIFxuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluNFwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIHRoaXMuZGlzYWJsZV9zaWduaW49dHJ1ZTtcbiAgICB9ICAgIGVsc2UgaWYodmFsPT1cImRpc3BsYXlMaXN0VXNlclwiKXtcbiAgICAgIGNvbnNvbGUubG9nKFwiU2lnbklOVXNlclwiKTtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiU2lnbkluVXNlclwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZGlzcGxheUxpc3RcIikuc3R5bGUuZGlzcGxheT0nJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiRW1wbG95ZWVTaWduSW5cIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4xXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMlwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjNcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW40XCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgdGhpcy5kaXNhYmxlX3NpZ25pbj10cnVlO1xuICAgIH1lbHNlIGlmKHZhbD09XCJFbXBsb3llZVNpZ25JblwiKXtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiU2lnbkluVXNlclwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZGlzcGxheUxpc3RcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkVtcGxveWVlU2lnbkluXCIpLnN0eWxlLmRpc3BsYXk9Jyc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4xXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMlwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjNcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW40XCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgIH1cbiAgICBlbHNlIGlmKHZhbD09XCJDb21wYW55U2lnbkluMVwiKXtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiU2lnbkluVXNlclwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZGlzcGxheUxpc3RcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkVtcGxveWVlU2lnbkluXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMVwiKS5zdHlsZS5kaXNwbGF5PScnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMlwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjNcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW40XCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgIH1cblxuICAgIGVsc2UgaWYodmFsPT1cIkNvbXBhbnlTaWduSW4yXCIpe1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJTaWduSW5Vc2VyXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJkaXNwbGF5TGlzdFwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiRW1wbG95ZWVTaWduSW5cIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4xXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMlwiKS5zdHlsZS5kaXNwbGF5PScnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluM1wiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjRcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgfVxuXG4gICAgZWxzZSBpZih2YWw9PVwiQ29tcGFueVNpZ25JbjNcIil7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIlNpZ25JblVzZXJcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRpc3BsYXlMaXN0XCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJFbXBsb3llZVNpZ25JblwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjFcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4yXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluM1wiKS5zdHlsZS5kaXNwbGF5PScnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluNFwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICB9XG5cbiAgICBlbHNlIGlmKHZhbD09XCJDb21wYW55U2lnbkluNFwiKXtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiU2lnbkluVXNlclwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZGlzcGxheUxpc3RcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkVtcGxveWVlU2lnbkluXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluMVwiKS5zdHlsZS5kaXNwbGF5PSdub25lJztcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiQ29tcGFueVNpZ25JbjJcIikuc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIkNvbXBhbnlTaWduSW4zXCIpLnN0eWxlLmRpc3BsYXk9J25vbmUnO1xuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJDb21wYW55U2lnbkluNFwiKS5zdHlsZS5kaXNwbGF5PScnO1xuICAgIH1cbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL3BhZ2VzL2xvZ2luL2xvZ2luLnRzIl0sInNvdXJjZVJvb3QiOiIifQ==