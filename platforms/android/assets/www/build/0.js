webpackJsonp([0],{

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestPageModule", function() { return TestPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__test__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_services_toast__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_sqlite_db_sqlite_db__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_unique_device_id__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var TestPageModule = (function () {
    function TestPageModule() {
    }
    return TestPageModule;
}());
TestPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__test__["a" /* TestPage */]),
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_services_toast__["a" /* Toastservice */], __WEBPACK_IMPORTED_MODULE_4__providers_sqlite_db_sqlite_db__["a" /* SqliteDbProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_unique_device_id__["a" /* UniqueDeviceID */]]
    })
], TestPageModule);

//# sourceMappingURL=test.module.js.map

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wYWdlcy90ZXN0L3Rlc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQXlDO0FBQ087QUFDZDtBQUMwQjtBQUNRO0FBQ0o7QUFVaEUsSUFBYSxjQUFjO0lBQTNCO0lBQTZCLENBQUM7SUFBRCxxQkFBQztBQUFELENBQUM7QUFBakIsY0FBYztJQVQxQix1RUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osdURBQVE7U0FDVDtRQUNELE9BQU8sRUFBRTtZQUNQLHNFQUFlLENBQUMsUUFBUSxDQUFDLHVEQUFRLENBQUM7U0FDbkM7UUFDRCxTQUFTLEVBQUMsQ0FBQywrRUFBWSxFQUFDLHdGQUFnQixFQUFDLHNGQUFjLENBQUM7S0FDekQsQ0FBQztHQUNXLGNBQWMsQ0FBRztBQUFIIiwiZmlsZSI6IjAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uaWNQYWdlTW9kdWxlIH0gZnJvbSAnaW9uaWMtYW5ndWxhcic7XG5pbXBvcnQgeyBUZXN0UGFnZSB9IGZyb20gJy4vdGVzdCc7XG5pbXBvcnQge1RvYXN0c2VydmljZX0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3NlcnZpY2VzL3RvYXN0JztcbmltcG9ydCB7U3FsaXRlRGJQcm92aWRlcn0gZnJvbSAnLi4vLi4vcHJvdmlkZXJzL3NxbGl0ZS1kYi9zcWxpdGUtZGInXG5pbXBvcnQgeyBVbmlxdWVEZXZpY2VJRCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvdW5pcXVlLWRldmljZS1pZCc7XG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBUZXN0UGFnZSxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIElvbmljUGFnZU1vZHVsZS5mb3JDaGlsZChUZXN0UGFnZSksXG4gIF0sXG4gIHByb3ZpZGVyczpbVG9hc3RzZXJ2aWNlLFNxbGl0ZURiUHJvdmlkZXIsVW5pcXVlRGV2aWNlSURdXG59KVxuZXhwb3J0IGNsYXNzIFRlc3RQYWdlTW9kdWxlIHt9XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvcGFnZXMvdGVzdC90ZXN0Lm1vZHVsZS50cyJdLCJzb3VyY2VSb290IjoiIn0=